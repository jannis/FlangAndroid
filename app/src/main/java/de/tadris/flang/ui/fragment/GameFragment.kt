package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import de.tadris.flang.game.GameController
import de.tadris.flang.R
import de.tadris.flang.audio.AudioController
import de.tadris.flang.databinding.FragmentGameBinding
import de.tadris.flang.ui.board.AnnotationFieldView
import de.tadris.flang.ui.board.ArrowFieldView
import de.tadris.flang.ui.board.BoardMoveDetector
import de.tadris.flang.ui.board.BoardView
import de.tadris.flang.game.ComputerHints
import de.tadris.flang.game.OnlineGameController
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.ui.PlayerViewController
import de.tadris.flang.util.Positions
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.Move
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

abstract class GameFragment : Fragment(R.layout.fragment_game),
    GameController.GameControllerCallback,
    BoardMoveDetector.MoveListener,
    ComputerHints.HintListener {

    private var _binding: FragmentGameBinding? = null
    protected val binding get() = _binding!!

    lateinit var boardView: BoardView

    protected var baseBoard = Board()
        set(value) {
            field = value
            hasCustomBaseBoard = true
        }
    protected var hasCustomBaseBoard = false
    protected var gameBoard = baseBoard.asBaseBoardForNewBoard()
    protected var displayedBoard = gameBoard.clone(true)

    private var isParticipant = false
    protected var isBoardDisabled = false

    private var color: Color? = null
    protected lateinit var player1ViewController: PlayerViewController
    protected lateinit var player2ViewController: PlayerViewController
    private var moveDetector: BoardMoveDetector? = null

    private var hintsEnabled = false
    private lateinit var hints: ComputerHints
    protected lateinit var positions: Positions

    protected lateinit var gameController: GameController

    protected var lastGameInfo: GameInfo? = null

    private val boardChangeListeners = mutableListOf<BoardChangeListener>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AudioController.getInstance(requireContext()) // Init sounds

        hints = ComputerHints(this)
        gameController = createGameController()
        positions = Positions(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!
        _binding = FragmentGameBinding.bind(root)

        boardView = BoardView(root.findViewById(R.id.mainBoard), displayedBoard, isClickable = true, animate = true)

        player1ViewController = PlayerViewController(Color.BLACK,
                binding.player1Name,
                binding.player1Title,
                binding.player1Rating,
                binding.player1RatingDiff,
                binding.player1TimeParent,
                binding.player1Time)
        player2ViewController = PlayerViewController(Color.BLACK,
                binding.player2Name,
                binding.player2Title,
                binding.player2Rating,
                binding.player2RatingDiff,
                binding.player2TimeParent,
                binding.player2Time)

        binding.resignButton.setOnClickListener {
            gameController.resignGame()
        }
        binding.resignButton.visibility = View.GONE

        binding.shareButton.setOnClickListener {
            showShareOptions()
        }
        binding.analysisButton.setOnClickListener {
            openAnalysis()
        }
        binding.computerAnalysisButton.setOnClickListener {
            openAnalysis()
        }
        binding.backButton.setOnClickListener {
            back()
        }
        binding.backButton.setOnLongClickListener {
            backToStart()
            true
        }
        binding.forwardButton.setOnClickListener {
            forward()
        }
        binding.forwardButton.setOnLongClickListener {
            setDisplayedBoardToGameBoard(true)
            true
        }
        binding.hintButton.setOnClickListener {
            toggleComputerHints()
        }
        binding.hintButton.visibility = if(gameController.isCreativeGame()) View.VISIBLE else View.GONE
        binding.swapSidesButton.setOnClickListener {
            swapSides()
        }
        binding.openingDatabaseToggleButton.visibility = View.GONE

        gameController.registerCallback(this)

        if(lastGameInfo == null){
            gameController.requestGame()
        }else{
            gameController.resume()
            onGameRequestSuccess(lastGameInfo!!, isParticipant, color, gameBoard)
        }

        lifecycleScope.launch {
            delay(500)
            if(isResumed){
                reinitBoard()
            }
        }

        refreshBoardView()

        return root
    }

    override fun onDestroyView() {
        gameController.stop()
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        runClockThread()
        super.onResume()
        reinitBoard()
    }

    private fun runClockThread(){
        thread {
            while (isResumed){
                activity?.runOnUiThread {
                    updateClocks()
                }
                Thread.sleep(500)
            }
        }
    }

    private fun updatePlayerInfo(){
        player1ViewController.color = if(boardView.isFlipped()) Color.WHITE else Color.BLACK
        player2ViewController.color = player1ViewController.color.getOpponent()

        if(lastGameInfo != null){
            player1ViewController.update(lastGameInfo!!)
            player2ViewController.update(lastGameInfo!!)

            // Viewers that are not players
            val gameRunning = lastGameInfo!!.running || lastGameInfo!!.lastAction > System.currentTimeMillis() - 30_000
            val realSpectators = lastGameInfo!!.spectatorCount - (if(gameRunning) 2 else 0)
            // Minimum viewer count to display the count
            val minSpectators = if(gameRunning) 1 else 2
            binding.spectatorCountRoot.visibility = if(realSpectators >= minSpectators) View.VISIBLE else View.INVISIBLE
            binding.spectatorCount.text = realSpectators.toString()
        }

        updateClocks()
    }

    private fun updateClocks(){
        if(lastGameInfo != null){
            player1ViewController.updateClock(lastGameInfo!!.configuration.infiniteTime, lastGameInfo!!.running && gameBoard.atMove == player1ViewController.color)
            player2ViewController.updateClock(lastGameInfo!!.configuration.infiniteTime, lastGameInfo!!.running && gameBoard.atMove == player2ViewController.color)
        }
    }

    override fun onGameRequestSuccess(info: GameInfo, isParticipant: Boolean, color: Color?, board: Board?) {
        this.gameBoard = board ?: info.toBoard(baseBoard)
        this.lastGameInfo = info
        this.color = color
        this.isParticipant = isParticipant
        boardView.setFlipped(color == Color.BLACK)
        if(isParticipant && info.running){
            moveDetector = BoardMoveDetector(requireContext(), boardView, color, this)
            boardView.listener = moveDetector
            binding.resignButton.visibility = View.VISIBLE
            AudioController.getInstance(requireContext()).playSound(AudioController.SOUND_NOTIFY_GENERIC)
        }else{
            binding.resignButton.visibility = View.GONE
        }
        updatePlayerInfo()
        setDisplayedBoardToGameBoard(true)
    }

    override fun onGameRequestFailed(reason: String) {
        Toast.makeText(requireContext(), reason, Toast.LENGTH_LONG).show()
    }

    override fun onMoveRequested(move: Move) {
        var boardRequest: Board? = null
        if(!isDisplayedBoardGameBoard()){
            if(gameController.isCreativeGame()){
                gameBoard = displayedBoard.clone(true)
                boardRequest = gameBoard.clone(true)
            }else{
                return
            }
        }
        moveDetector?.setAllowed(false)
        gameController.onMoveRequested(move, boardRequest)
    }

    override fun onUpdate(action: Action) {
        val isOverBefore = gameBoard.gameIsComplete()
        val isCapture = action is Move && action.target.onDifferentBoard(gameBoard).piece.value != null

        action.applyToBoard(gameBoard)

        val isOverAfter = gameBoard.gameIsComplete()

        if(context != null){
            if(isOverAfter && !isOverBefore){
                AudioController.getInstance(requireContext()).playSound(AudioController.SOUND_NOTIFY_GENERIC)
                @StringRes var message = R.string.gameEndUnknown
                if(lastGameInfo != null && !lastGameInfo!!.configuration.infiniteTime && (lastGameInfo!!.white.time < 0 || lastGameInfo!!.black.time < 0)){
                    message = R.string.gameEndTimeout
                }else if(gameBoard.resigned != null){
                    message = R.string.gameEndResign
                }else if(gameBoard.hasWon(Color.WHITE) || gameBoard.hasWon(Color.BLACK)){
                    message = R.string.gameEndFlang
                }
                boardView.showMessage(getString(message), 2000)
            }else if(action is Move){
                AudioController.getInstance(requireContext()).playSound(
                    if(isCapture){ AudioController.SOUND_MOVE_CAPTURE } else { AudioController.SOUND_MOVE }
                )
            }
        }
        setDisplayedBoardToGameBoard(true)

        if(gameBoard.gameIsComplete()){
            binding.resignButton.visibility = View.GONE
        }

        updateClocks()
    }

    override fun onUpdate(gameInfo: GameInfo) {
        this.lastGameInfo = gameInfo
        updatePlayerInfo()
    }

    protected fun setDisplayedBoardToGameBoard(force: Boolean){
        if(!force && !isDisplayedBoardGameBoard()){ return }
        displayedBoard = gameBoard.clone(true)
        refreshBoardView()
    }

    protected fun refreshBoardView(){
        if(_binding == null) return
        boardView.refreshBoard(displayedBoard)
        moveDetector?.setAllowed(movesAllowed())
        binding.backButton.isEnabled = displayedBoard.moveList.size > 0
        binding.forwardButton.isEnabled = !isDisplayedBoardGameBoard()
        binding.positionName.text = positions.findPositionName(displayedBoard) ?: ""
        requestHintsIfEnabled()
        boardChangeListeners.forEach {
            it.onDisplayedBoardChange(displayedBoard)
        }
    }

    private fun movesAllowed(): Boolean {
        return when {
            isBoardDisabled -> {
                false
            }
            gameController.isCreativeGame() -> {
                true
            }
            else -> {
                isDisplayedBoardGameBoard() && !gameBoard.gameIsComplete() && (color == null || color == gameBoard.atMove)
            }
        }
    }

    private fun backToStart(){
        displayedBoard = baseBoard.asBaseBoardForNewBoard()
        refreshBoardView()
    }

    private fun back(){
        val moveList = displayedBoard.moveList
        println("undoing: " + moveList.rewind())
        displayedBoard = moveList.board
        refreshBoardView()
    }

    private fun forward(){
        val index = displayedBoard.moveList.size
        val action = gameBoard.moveList[index]
        println("redoing: $action")
        action.applyToBoard(displayedBoard)
        refreshBoardView()
    }

    private fun isDisplayedBoardGameBoard(): Boolean{
        return displayedBoard.moveNumber == gameBoard.moveNumber
    }

    private fun openAnalysis(){
        val bundle = Bundle()
        bundle.putString(AbstractAnalysisGameFragment.ARGUMENT_BOARD_FMN, displayedBoard.getFMN())
        bundle.putBoolean(AbstractAnalysisGameFragment.ARGUMENT_RUNNING_GAME, !gameBoard.gameIsComplete() && !gameController.isCreativeGame())
        bundle.putBoolean(AbstractAnalysisGameFragment.ARGUMENT_FLIPPED, boardView.isFlipped())
        findNavController().navigate(getNavigationLinkToAnalysis(), bundle)
    }

    @IdRes
    protected abstract fun getNavigationLinkToAnalysis(): Int

    @IdRes
    protected abstract fun getNavigationLinkToChat(): Int

    private fun swapSides(){
        boardView.setFlipped(!boardView.isFlipped())
        reinitBoard()
        updatePlayerInfo()
    }

    private fun reinitBoard(){
        boardView.setBoard(displayedBoard)
        refreshBoardView()
        val charsY = if(boardView.isFlipped()) Board.BOARD_SIZE-1 else 0
        val numbersX = if(boardView.isFlipped()) 0 else Board.BOARD_SIZE-1
        for(x in 0 until Board.BOARD_SIZE){
            val location = Location(displayedBoard, x, charsY)
            val view = AnnotationFieldView(requireContext(), location, ('A'.toInt() + x).toChar().toString())
            view.setTextColor(resources.getColor(if((x % 2 == 1).xor(boardView.isFlipped())) R.color.boardBlack else R.color.boardWhite))
            view.textSize = 12f
            boardView.attach(view)
        }
        val minY = if(boardView.isFlipped()) 0 else 1
        val maxY = Board.BOARD_SIZE - if(boardView.isFlipped()) 1 else 0
        for(y in minY until maxY){
            val location = Location(displayedBoard, numbersX, y)
            val view = AnnotationFieldView(requireContext(), location, (y + 1).toString())
            view.setTextColor(resources.getColor(if((y % 2 == 0).xor(boardView.isFlipped())) R.color.boardBlack else R.color.boardWhite))
            view.textSize = 12f
            boardView.attach(view)
        }
    }

    private fun toggleComputerHints(){
        clearHints()
        hintsEnabled = !hintsEnabled
        Toast.makeText(requireContext(), if(hintsEnabled) R.string.hintsEnabled else R.string.hintsDisabled, Toast.LENGTH_SHORT).show()
        requestHintsIfEnabled()
    }

    private fun requestHintsIfEnabled(){
        if(hintsEnabled){
            requestHints()
        }
    }

    private fun requestHints(){
        hints.requestHints(displayedBoard.clone(true))
        clearHints()
    }

    private fun clearHints(){
        boardView.detachAllArrows()
    }

    override fun onHintsResult(hints: List<ComputerHints.ComputerHint>) {
        activity?.runOnUiThread {
            hints.forEach {
                boardView.attach(ArrowFieldView(context, it.move, boardView, it.color))
            }
        }
    }

    private fun showShareOptions(){
        if(hasCustomBaseBoard){
            copyToClipboard(displayedBoard.getFBN2())
            return
        }
        val arrayAdapter = ArrayAdapter<String>(requireActivity(), android.R.layout.select_dialog_item)
        arrayAdapter.add(getString(R.string.copyFBN))
        arrayAdapter.add(getString(R.string.copyFMN))
        arrayAdapter.add(getString(R.string.shareGame))
        if(displayedBoard.getFMN().isNotEmpty()){
            arrayAdapter.add(getString(R.string.sendToGlobalChat))
        }
        AlertDialog.Builder(activity)
            .setAdapter(arrayAdapter) { _, which ->
                when(which){
                    0 -> copyToClipboard(displayedBoard.getFBN2())
                    1 -> copyToClipboard(displayedBoard.getFMN2())
                    2 -> showShareDialog()
                    3 -> sendToGlobalChat()
                    else -> throw IllegalArgumentException("Action $which is not defined")
                }
            }
            .show()
    }

    private fun showShareDialog(){
        val editText = EditText(requireContext())
        AlertDialog.Builder(requireContext())
                .setTitle(R.string.dialogShareTitle)
                .setMessage(R.string.dialogEnterNameMessage)
                .setView(editText)
                .setPositiveButton(R.string.actionShare) { _, _ ->
                    shareBoard(editText.text.toString())
                }
                .setNegativeButton(R.string.actionCancel, null)
                .show()
    }

    private fun shareBoard(name: String){
        val text = "$name\n\n${displayedBoard.getFMN2()}\n\n${displayedBoard.getFBN2()}"

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(intent)
    }

    private fun copyToClipboard(str: String){
        val clipboard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText(str, str)
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip)
            Toast.makeText(context, R.string.copiedToClipboard, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, R.string.copyingFailed, Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendToGlobalChat(){
        val controller = gameController
        val bundle = Bundle()
        if(controller is OnlineGameController){
            bundle.putString(ChatFragment.ARGUMENT_GAME_ID, controller.gameId.toString())
        }
        bundle.putString(ChatFragment.ARGUMENT_FMN, displayedBoard.getFMN2())
        findNavController().navigate(getNavigationLinkToChat(), bundle)
    }

    abstract fun createGameController(): GameController

    fun registerBoardChangeListener(listener: BoardChangeListener){
        boardChangeListeners.add(listener)
    }

    interface BoardChangeListener {

        fun onDisplayedBoardChange(board: Board)

    }

}