package de.tadris.flang.game

import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.User
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.Move

interface GameController {

    fun registerCallback(callback: GameControllerCallback)

    // If the move is played on a different board that the game controller knows of, newBoardRequest is not null
    // This happens in creative games when the user is replaying moves
    fun onMoveRequested(move: Move, newBoardRequest: Board?)

    fun requestGame()

    fun resignGame()

    fun isCreativeGame(): Boolean

    fun stop()

    fun resume()

    interface GameControllerCallback {

        fun onGameRequestSuccess(info: GameInfo, isParticipant: Boolean, color: Color?, board: Board? = null)

        fun onGameRequestFailed(reason: String)

        fun onUpdate(gameInfo: GameInfo)

        fun onUpdate(action: Action)

    }

}