package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Type
import de.tadris.flang_lib.Vector

class ActionList(board: Board? = null, var base: Board? = null) {

    var board = board ?: base?.asBaseBoardForNewBoard() ?: Board(Board.DEFAULT_BOARD)

    private val actionList = mutableListOf<Action>()

    val actions get() = actionList as List<Action>
    val size get() = actionList.size

    constructor(actionList: MutableList<Action>) : this(){
        this.actionList.addAll(actionList)
        regenerateBoard()
    }

    fun addMoveList(str: String): ActionList {
        str.split(" ").forEach {
            addMove(it)
        }
        return this
    }

    fun addMove(str: String): ActionList {
        if(str.isEmpty()) return this
        val action = parseAction(str)
        if(action != null){
            addMove(action)
        }
        return this
    }

    fun addMoves(moves: List<Action>){
        moves.forEach {
            addMove(it)
        }
    }

    fun addMove(action: Action){
        action.applyToBoard(board)
        actionList.add(action)
    }

    fun addMoveWithoutApply(action: Action){
        actionList.add(action)
    }

    fun removeLastMoveWithoutApply(){
        actionList.removeLast()
    }

    fun parseAction(actionStr: String): Action {
        return when(actionStr[0]){
            '#' -> Resign(actionStr)
            else -> Move.parse(board, actionStr)
        }
    }

    fun regenerateBoard(): Board {
        board = base?.asBaseBoardForNewBoard() ?: Board()
        actionList.forEach {
            it.applyToBoard(board)
        }
        return board
    }

    fun rewind(): Action {
        val move = actionList.removeLast()
        regenerateBoard()
        return move
    }

    fun reset(){
        actionList.clear()
        regenerateBoard()
    }

    fun clone(): ActionList {
        val l = ActionList(board, base)
        l.actionList.addAll(actionList)
        return l
    }

    fun skipTo(index: Int){
        actions.dropLast(actions.size - index)
        regenerateBoard()
    }

    inline fun forEachGameState(action: (board: Board, nextAction: Action?) -> Unit){
        val list = ActionList()
        actions.forEach {
            action(list.board, it)
            list.addMove(it)
        }
        action(list.board, null)
    }

    operator fun get(index: Int) = actionList[index]

    override fun toString(): String {
        return actionList.joinToString(separator = " ")
    }

    fun toShortString(): String {
        val builder = StringBuilder()
        forEachGameState { board, nextAction ->
            if(nextAction != null){
                builder.append(nextAction.getShortNotation(board))
                builder.append(" ")
            }
        }
        return builder.toString().trim()
    }

}