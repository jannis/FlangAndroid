package de.tadris.flang_lib.bot

import de.tadris.flang_lib.*
import kotlin.math.pow

/**
 * Works similar to the NeoBoardEvaluation
 */
class BlankBoardEvaluation(board: Board = Board()) : NeoBoardEvaluation(board) {

    companion object {

        val defenceYWeightsForWhiteKing = arrayOf(
            4 to 2.0,
            5 to 3.0,
            6 to 1.0,
        )
        val defenceYWeightsForBlackKing = arrayOf(
            3 to 2.0,
            2 to 3.0,
            1 to 1.0,
        )

    }

    override fun evaluate(): Double {
        var adder = 0.0
        var won = false
        if(board.hasWon(Color.WHITE)){
            adder = 10000.0
            won = true
        }
        if(board.hasWon(Color.BLACK)){
            adder = -10000.0
            won = true
        }
        prepare()
        board.eachXY { x, y ->
            evaluateLocation(x, y)
        }
        var evaluation = 0.0
        val movementEval = (whiteStats.movements.toDouble() / blackStats.movements) - (blackStats.movements.toDouble() / whiteStats.movements)
        val pieceValueEval = (whiteStats.pieceValue.toDouble() / blackStats.pieceValue) - (blackStats.pieceValue.toDouble() / whiteStats.pieceValue)
        kingsEvalNum = if(!won) getKingsEval() else 0.0
        val matrixEval = evaluationMatrix.sumOf { it!!.evaluateField() }

        evaluation+=   5 * matrixEval
        evaluation+=  10 * movementEval
        evaluation+=  60 * pieceValueEval
        evaluation+=   5 * kingsEvalNum

        return (evaluation / 10.0) + adder
    }

    override fun getKingsEval(): Double {
        val whiteKing = board.findKingIndex(Color.WHITE)
        val blackKing = board.findKingIndex(Color.BLACK)
        if(whiteKing == -1 || blackKing == -1){
            throw IllegalStateException("Cannot find kings in board $board")
        }
        val whiteKingX = Board.getXForIndex(whiteKing)
        val whiteKingY = Board.getYForIndex(whiteKing)
        val blackKingX = Board.getXForIndex(blackKing)
        val blackKingY = Board.getYForIndex(blackKing)

        val whiteEval = 20 * 2.0.pow(whiteKingY - 6)
        val blackEval = 20 * 2.0.pow(-blackKingY + 1)

        for(y in whiteKingY until (Board.BOARD_SIZE - 1)){
            val field = getAt(whiteKingX, y)
            field.weight+= whiteEval
        }
        for(y in blackKingY downTo 1){
            val field = getAt(blackKingX, y)
            field.weight+= blackEval
        }

        calculateDefenceWeightsFor(Color.WHITE, whiteKingX, whiteKingY, whiteEval)
        calculateDefenceWeightsFor(Color.BLACK, blackKingX, blackKingY, blackEval)

        return (whiteEval / blackEval) - (blackEval / whiteEval)
    }

    private fun calculateDefenceWeightsFor(kingColor: Color, kingX: Int, kingY: Int, bonus: Double){
        var minX = kingX - 1
        var maxX = kingX + 1
        if(minX < 0){
            minX++
        }
        if(maxX >= Board.BOARD_SIZE){
            maxX--
        }
        for(x in minX..maxX){
            val yWeights =
                if(kingColor == Color.WHITE) defenceYWeightsForWhiteKing
                else defenceYWeightsForBlackKing
            yWeights.forEach {
                val y = it.first
                getAt(x, y).bonus+= it.second * bonus * 5
            }
        }
    }

}