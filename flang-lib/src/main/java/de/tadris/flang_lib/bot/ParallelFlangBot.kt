package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.math.*

class ParallelFlangBot(
    val minDepth: Int,
    val maxDepth: Int,
    val evaluationFactory: () -> BoardEvaluation,
    threads: Int = Runtime.getRuntime().availableProcessors()
) {

    val lock = Object()
    var totalEvaluations = 0
    private val executors = Executors.newFixedThreadPool(threads)
    val moveEvaluations = mutableListOf<MoveEvaluation>()

    fun findBestMove(board: Board, printTime: Boolean = true) : BotResult {
        val start = System.currentTimeMillis()
        val eval = findBestMove(board, maxDepth)
        val end = System.currentTimeMillis()
        if(printTime){
            println((end-start).toString() + "ms")
        }
        return eval!!
    }

    private fun findBestMove(board: Board, depth: Int) : BotResult? {
        moveEvaluations.clear()

        val allMoves = board.findAllMoves(board.atMove).toMutableList()
        allMoves.sortBy { NeoBoardEvaluation(board.executeOnNewBoard(it)).evaluate()*board.atMove.evaluationNumber }
        allMoves.forEach {
            executors.submit(FlangBotThread(board, it, depth - 1, evaluationFactory()))
        }

        executors.shutdown()
        executors.awaitTermination(1, TimeUnit.HOURS)

        moveEvaluations.shuffle()
        moveEvaluations.sortBy { -((it.evaluation*100).roundToInt() / 100.0)*board.atMove.evaluationNumber }
        if(moveEvaluations.size == 0){
            return null
        }
        val bestMove = moveEvaluations[0]

        return BotResult(MoveEvaluation(bestMove.move, bestMove.evaluation, depth), moveEvaluations, totalEvaluations)
    }

    inner class FlangBotThread(val board: Board, val move: Move, val depth: Int, val evaluation: BoardEvaluation) : Runnable {

        private val moveGenerator = FastMoveGenerator(Board(), false)
        private var evaluationCount = 0

        override fun run() {
            val evaluation = MoveEvaluation(move, evaluateMove(board.executeOnNewBoard(move), depth, -100000.0, 100000.0), depth)
            synchronized(lock){
                moveEvaluations += evaluation
                totalEvaluations += evaluationCount
            }
        }

        private fun evaluateMove(board: Board, depth: Int, alpha: Double, beta: Double) : Double {
            var alpha = alpha
            var beta = beta
            if(depth <= 0){
                evaluationCount++
                return evaluation.evaluate(board)
            }

            var bestEvaluation = -1000000.0*board.atMove.evaluationNumber

            moveGenerator.board = board
            val allMoves = mutableListOf<Move>()
            moveGenerator.forEachMove(board.atMove) { allMoves.add(it) }
            if(depth >= 3){
                val preEvals = allMoves.associateWith { -evaluateMove(board.executeOnNewBoard(it), 0, 0.0, 0.0)*board.atMove.evaluationNumber }
                allMoves.sortBy { preEvals[it] }
            }else if(depth >= 2){
                allMoves.sortBy {
                    it.piece.location.y * it.piece.color.evaluationNumber
                }
            }

            allMoves.forEach { move ->
                val revertInfo = board.revertableExecuteOnBoard(move) // make move
                val rawMoveEvaluation = evaluateMove(board, depth - 1, alpha, beta) // go down in tree
                board.revertMove(move, revertInfo) // revert move

                val finalMoveEvaluation =
                    if(rawMoveEvaluation.absoluteValue > 9000) rawMoveEvaluation * 0.99
                    else rawMoveEvaluation

                if(board.atMove == Color.WHITE){
                    bestEvaluation = max(bestEvaluation, finalMoveEvaluation)
                    alpha = max(alpha, bestEvaluation)
                    if(alpha >= beta){
                        return bestEvaluation
                    }
                }else{
                    bestEvaluation = min(bestEvaluation, finalMoveEvaluation)
                    beta = min(beta, bestEvaluation)
                    if(beta <= alpha){
                        return bestEvaluation
                    }
                }
            }

            return bestEvaluation
        }

    }

    class BotResult(val bestMove: MoveEvaluation, val evaluations: List<MoveEvaluation>, val count: Int){

        fun getVariance(): Double {
            val sum = evaluations.sumOf { it.evaluation }
            val avg = sum / evaluations.size
            val summedDiff = evaluations.sumOf { (it.evaluation - avg).pow(2) }
            return summedDiff / evaluations.size
        }

    }

}