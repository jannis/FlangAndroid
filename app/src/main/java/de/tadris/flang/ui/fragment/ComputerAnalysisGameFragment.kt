package de.tadris.flang.ui.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.WorkerThread
import androidx.lifecycle.lifecycleScope
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import de.tadris.flang.R
import de.tadris.flang.databinding.ViewComputerAnalysisBinding
import de.tadris.flang.ui.board.AnnotationFieldView
import de.tadris.flang.ui.board.ArrowFieldView
import de.tadris.flang.ui.view.ChartFormatter
import de.tadris.flang.util.getThemePrimaryColor
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Vector
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.analysis.*
import de.tadris.flang_lib.bot.BlankBoardEvaluation
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import de.tadris.flang_lib.bot.StageEvaluation2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.max
import kotlin.math.roundToInt

class ComputerAnalysisGameFragment : AbstractAnalysisGameFragment(), AnalysisListener,
    OnChartValueSelectedListener {

    private var _analysisBinding: ViewComputerAnalysisBinding? = null
    private val analysisBinding get() = _analysisBinding!!

    private var state = AnalysisUIState.NO_ANALYSIS_AVAILABLE

    private var analysisResult: AnalysisResult? = null

    private var lastHighlight = -1f

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        binding.gameAdditionalContentParent.visibility = View.VISIBLE
        binding.hintButton.visibility = View.GONE

        _analysisBinding = ViewComputerAnalysisBinding.inflate(inflater, binding.gameAdditionalContentParent, true)

        analysisBinding.requestAnalysisButton.setOnClickListener {
            beginAnalysis()
        }

        with(analysisBinding.computerAnalysisChart){
            with(ChartFormatter){
                initChart(requireActivity())
            }
            setOnChartValueSelectedListener(this@ComputerAnalysisGameFragment)
        }

        boardView.listener = null // disable user interaction

        if(state == AnalysisUIState.NO_ANALYSIS_AVAILABLE){
            beginAnalysis() // automatic analysis, can be remove if there is a selection
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        refreshUI()
    }

    override fun onDestroyView() {
        _analysisBinding = null
        super.onDestroyView()
    }

    private fun refreshUI(){
        when(state){
            AnalysisUIState.NO_ANALYSIS_AVAILABLE -> {
                analysisBinding.computerAnalysisChartParent.visibility = View.GONE
                analysisBinding.requestAnalysisButton.visibility = View.VISIBLE
                analysisBinding.analyzingParent.visibility = View.GONE
            }
            AnalysisUIState.ANALYZING -> {
                analysisBinding.analyzingParent.visibility = View.VISIBLE
                analysisBinding.computerAnalysisChart.alpha = 0.3f
                analysisBinding.requestAnalysisButton.visibility = View.GONE
            }
            AnalysisUIState.SHOW_ANALYSIS -> {
                analysisBinding.requestAnalysisButton.visibility = View.GONE
                analysisBinding.computerAnalysisChart.alpha = 1f
                analysisBinding.analyzingParent.visibility = View.GONE
            }
        }
        if(analysisResult != null){
            analysisBinding.computerAnalysisChartParent.visibility = View.VISIBLE
            showChart()
            onDisplayedBoardChange(displayedBoard)
        }
    }

    private fun beginAnalysis(){
        val depthTries = listOf(1, 2, 3)
        lifecycleScope.launch {
            state = AnalysisUIState.ANALYZING
            depthTries.forEach { depth ->
                refreshUI()
                delay(500)
                analyze(depth)
            }
            state = AnalysisUIState.SHOW_ANALYSIS
            refreshUI()
        }
    }

    private fun showChart(){
        val chart = analysisBinding.computerAnalysisChart

        val evalFutureDataSet = getDataSet(
            entryMapper = { eval, last ->
                (eval.bestBoardEval + last.bestBoardEval).toFloat() / 2
            },
            color = requireContext().resources.getColor(R.color.green_700)
        )

        val scoreData = LineData(evalFutureDataSet)
        scoreData.setDrawValues(false)

        chart.description.text = ""
        chart.data = scoreData
        chart.legend.setCustom(emptyArray())

        chart.axisLeft.axisMinimum = -22f
        chart.axisLeft.axisMaximum = 22f
        chart.axisRight.axisMinimum = -22f
        chart.axisRight.axisMaximum = 22f

        chart.isHighlightPerDragEnabled = true
        chart.isHighlightPerTapEnabled = true

        chart.notifyDataSetChanged()
        chart.invalidate()
    }

    private fun getDataSet(entryMapper: (eval: AnalysisMoveEvaluation, last: AnalysisMoveEvaluation) -> Float, color: Int): LineDataSet {
        val result = analysisResult!!
        val entries = result.moveEvaluations.mapIndexed { index, eval ->
            Entry((index).toFloat(), entryMapper(eval, result.moveEvaluations[max(0, index - 1)]).coerceIn(-20f, 20f), eval)
        }
        val dataSet = LineDataSet(entries, "")
        with(dataSet){
            setDrawCircles(false)
            this.color = color
            lineWidth = 3f
            highLightColor = requireActivity().getThemePrimaryColor()
            highlightLineWidth = 2f
        }
        return dataSet
    }

    override fun onDisplayedBoardChange(board: Board) {
        if(_analysisBinding != null && analysisBinding.computerAnalysisChart.data != null){
            analysisBinding.computerAnalysisChart.highlightValue(board.moveList.size.toFloat(), 0)
        }
        refreshBreakdown()
    }

    override fun isGameClickable() = true

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_computer_analysis_to_nav_analysis
    override fun getNavigationLinkToChat() = R.id.action_nav_computer_analysis_to_nav_chat

    @WorkerThread
    private suspend fun analyze(depth: Int) = withContext(Dispatchers.IO){
        analysisResult = ComputerAnalysis(fmn, depth, this@ComputerAnalysisGameFragment).analyze()
    }

    enum class AnalysisUIState {
        NO_ANALYSIS_AVAILABLE,
        ANALYZING,
        SHOW_ANALYSIS
    }

    override fun onProgressChanged(progress: Int) {
        if(_analysisBinding == null) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            analysisBinding.analyzingProgress.setProgress(progress, true)
        }else{
            analysisBinding.analyzingProgress.progress = progress
        }
    }

    override fun onValueSelected(e: Entry, h: Highlight) {
        if(h.x != lastHighlight){
            lastHighlight = h.x
            val board = ActionList(gameBoard.moveList.actions.subList(0, h.x.toInt()).toMutableList()).board
            displayedBoard = board
            refreshBoardView()

            boardView.detachAllArrows()
            val nextAction = gameBoard.moveList.actions.getOrNull(h.x.toInt())
            if(nextAction != null && nextAction is Move){
                showArrow(e.data as AnalysisMoveEvaluation, board, nextAction)
            }
        }
    }

    private fun showArrow(eval: AnalysisMoveEvaluation, board: Board, move: Move){
        val color = requireContext().resources.getColor(
            when(eval.type){
                AnalysisMoveType.GOOD -> R.color.green_700
                AnalysisMoveType.MISTAKE -> R.color.yellow_700
                AnalysisMoveType.BLUNDER -> R.color.red_700
                AnalysisMoveType.RESIGN -> R.color.red_700
            }
        )
        boardView.attach(ArrowFieldView(context, move, boardView, color))
        if(eval.type != AnalysisMoveType.GOOD && eval.bestMove.isNotEmpty()){
            val bestMove = Move.parse(board, eval.bestMove)
            boardView.attach(ArrowFieldView(context, bestMove, boardView, requireContext().resources.getColor(R.color.green_700_faded)))
        }
    }

    override fun onNothingSelected() { }

    private fun refreshBreakdown(){
        val eval = StageEvaluation2(displayedBoard)
        val breakdown = eval.evaluateBreakdown()
        boardView.detachAllAnnotations()

        for(i in breakdown.evaluationMatrix.indices){
            val evaluation = breakdown.evaluationMatrix[i]!!
            val location = Vector(i % Board.BOARD_SIZE, i / Board.BOARD_SIZE).toLocation(displayedBoard)
            val annotation = AnnotationFieldView(requireContext(), location, (evaluation.evaluateField()).roundToInt().toString())
            val redChannel = ((evaluation.weight.toFloat() - 1) * 200).toInt().coerceIn(0, 255).shl(16)
            annotation.setTextColor((0x99000000).toInt().or(redChannel))
            boardView.attach(annotation)
        }

    }

}