package de.tadris.flang.network_api.exception

class ServerErrorException : ServerException("Internal Server Error", 500, null)