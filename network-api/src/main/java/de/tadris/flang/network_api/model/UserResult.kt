package de.tadris.flang.network_api.model

data class UserResult(val users: List<UserInfo>)