package de.tadris.flang_lib

import kotlin.math.max

object TimeUtils {

    fun getTimeAsString(time: Long): String {
        val timeLeft = max(time / 1000, 0)
        val min = timeLeft / 60
        val sec = timeLeft % 60
        return "$min:${if(sec < 10) "0" else ""}$sec"
    }

    enum class TimeControlZone(val displayName: String, val smallerThan: Long) {

        ULTRA_BULLET("Ultrabullet", 60_000),
        BULLET("Bullet", 60_000 * 3),
        BLITZ("Blitz", 60_000 * 10),
        RAPID("Rapid", 60_000 * 20),
        CLASSICAL("Classical", 60_000 * 60),
        INFINITE("Infinite", -1);

        companion object {
            fun getZone(infiniteTime: Boolean, time: Long) = when{
                infiniteTime -> INFINITE
                time < ULTRA_BULLET.smallerThan -> ULTRA_BULLET
                time < BULLET.smallerThan -> BULLET
                time < BLITZ.smallerThan -> BLITZ
                time < RAPID.smallerThan -> RAPID
                else -> CLASSICAL
            }
        }

    }

}