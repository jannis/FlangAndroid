package de.tadris.flang

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import de.tadris.flang_lib.bot.ParallelFlangBot
import org.junit.Test

class TestParallelFlangBot {

    @Test
    fun findBestMove2(){
        val board = Board()
        val bot = ParallelFlangBot(2, 2, { NeoBoardEvaluation() }, 1)
        val result = bot.findBestMove(board).bestMove
        println(result)
    }

    @Test
    fun findBestMove3(){
        val board = Board()
        val bot = ParallelFlangBot(3, 3, { NeoBoardEvaluation() }, 4)
        val result = bot.findBestMove(board).bestMove
        println(result)
    }

    @Test
    fun findBestMoveBlack(){
        val board = Board.fromFMN("PG2-G3 pB7-B6 FF1-G4 kA8-B7 PG3-H4 kB7-A6 PC1-B2 kA6-A5 KH1-G2 pB6-C5 PD2-C3 uB8-B2 PC2-B3 pC5-C4 PC3-C4 uB2-D1 HE1-C2 fC8-C4 UG1-D1 pC7-B6 FG4-C4 hD8-C6 UD1-D7 pB6-C5 FC4-C6 rE8-A8 KG2-H3 pC5-B4 HC2-B4 pA7-B6 KH3-G4")
        val bot = ParallelFlangBot(3, 3, { NeoBoardEvaluation() }, 8)
        val result = bot.findBestMove(board).bestMove
        println(result)
    }

}