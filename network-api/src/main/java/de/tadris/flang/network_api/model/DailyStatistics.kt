package de.tadris.flang.network_api.model

data class DailyStatistics(val stats: List<DailyStatsEntry>)