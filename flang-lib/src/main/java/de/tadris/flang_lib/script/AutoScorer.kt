package de.tadris.flang_lib.script

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.bot.*
import java.io.File
import java.util.concurrent.Executors
import kotlin.math.roundToInt

fun main(){

    println("Loading starting positions")
    val startingPositions = File("doc/startingPos.txt")
        .readLines()
        .map { Board.fromFMN(it) }

    val lock = Object()

    fun createBot1() = FlangBot(0, 3, evaluation = NeoBoardEvaluation())
    fun createBot2() = FlangBot(0, 3, evaluation = StageEvaluation2())

    var bot1WinsWhite = 0
    var bot1WinsBlack = 0
    var bot2WinsWhite = 0
    var bot2WinsBlack = 0
    var totalGames = 0

    fun makeGame(board: Board, white: FlangBot, black: FlangBot): Color? {
        while(!board.gameIsComplete()){
            val bot = if(board.atMove == Color.WHITE) white else black
            val move = bot.findBestMove(board, false).bestMove.move
            board.executeOnBoard(move)
            if(board.moveNumber > 100){
                println("\nMove number too high: ${board.getFMN2()}")
                break
            }
        }
        return board.getWinningColor()
    }

    fun test(board: Board){
        val game1 = makeGame(board.clone(true), createBot1(), createBot2())
        val game2 = makeGame(board.clone(true), createBot2(), createBot1())

        synchronized(lock){
            when(game1){
                Color.WHITE -> bot1WinsWhite++
                Color.BLACK -> bot2WinsBlack++
                null -> { }
            }
            when(game2){
                Color.WHITE -> bot2WinsWhite++
                Color.BLACK -> bot1WinsBlack++
                null -> { }
            }
            totalGames+= 2
        }
    }

    val executor = Executors.newFixedThreadPool(24)
    startingPositions.forEach {
        executor.submit {
            try {
                test(it)
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    print("Testing...")
    while (totalGames < startingPositions.size * 2){
        print("\rTesting $totalGames/${startingPositions.size * 2}...")
        Thread.sleep(1000)
    }
    executor.shutdown()

    println("\rFinished testing!    ")

    fun getScore(num: Int, total: Int) = (((num.toDouble() / total) - 0.5) * 2000).roundToInt() / 10.0

    println("Bot 1 score: ${getScore(bot1WinsWhite + bot1WinsBlack, totalGames)} white: ${getScore(bot1WinsWhite, totalGames / 2)} black: ${getScore(bot1WinsBlack, totalGames / 2)}")
    println("Bot 2 score: ${getScore(bot2WinsWhite + bot2WinsBlack, totalGames)} white: ${getScore(bot2WinsWhite, totalGames / 2)} black: ${getScore(bot2WinsBlack, totalGames / 2)}")

}