package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Type
import de.tadris.flang_lib.action.Move
import kotlin.math.*

class FlangBot(
    val minDepth: Int,
    val maxDepth: Int,
    val resetEvaluations: Boolean = true,
    val evaluation: BoardEvaluation = NeoBoardEvaluation(Board()),
) {

    constructor(depth: Int): this(depth, depth)

    val moveGenerator = FastMoveGenerator(Board(), false)

    var evaluations = 0

    fun findBestMove(board: Board, printTime: Boolean = true) : BotResult {
        val start = System.currentTimeMillis()
        if(resetEvaluations)
            evaluations = 0
        val eval = findBestMove(board, maxDepth)
        val end = System.currentTimeMillis()
        if(printTime){
            println((end-start).toString() + "ms")
        }
        return eval!!
    }

    private fun findBestMove(board: Board, depth: Int) : BotResult? {
        val moveEvaluations = mutableListOf<MoveEvaluation>()
        val allMoves = board.findAllMoves(board.atMove).toMutableList()
        allMoves.sortBy { NeoBoardEvaluation(board.executeOnNewBoard(it)).evaluate()*board.atMove.evaluationNumber }
        allMoves.forEach {
            moveEvaluations+= MoveEvaluation(it, evaluateMove(board.executeOnNewBoard(it), depth-1, -100000.0, 100000.0), depth)
        }

        moveEvaluations.shuffle()
        moveEvaluations.sortBy { -((it.evaluation*100).roundToInt() / 100.0)*board.atMove.evaluationNumber }
        if(moveEvaluations.size == 0){
            return null
        }
        val bestMove = moveEvaluations[0]

        return BotResult(MoveEvaluation(bestMove.move, bestMove.evaluation, depth), moveEvaluations, evaluations)
    }

    private fun evaluateMove(board: Board, depth: Int, alpha: Double, beta: Double) : Double {
        var alpha = alpha
        var beta = beta
        if(depth <= 0){
            evaluations++
            return evaluation.evaluate(board)
        }

        var bestEvaluation = -1000000.0*board.atMove.evaluationNumber

        moveGenerator.board = board
        val allMoves = mutableListOf<Move>()
        moveGenerator.forEachMove(board.atMove) { allMoves.add(it) }
        if(depth >= 3){
            val preEvals = allMoves.associateWith { -evaluateMove(board.executeOnNewBoard(it), 0, 0.0, 0.0)*board.atMove.evaluationNumber }
            allMoves.sortBy { preEvals[it] }
        }else if(depth >= 2){
            allMoves.sortBy {
                it.piece.location.y * it.piece.color.evaluationNumber
            }
        }

        allMoves.forEach { move ->
            val revertInfo = board.revertableExecuteOnBoard(move) // make move
            val rawMoveEvaluation = evaluateMove(board, depth - 1, alpha, beta) // go down in tree
            board.revertMove(move, revertInfo) // revert move

            val finalMoveEvaluation =
                if(rawMoveEvaluation.absoluteValue > 9000) rawMoveEvaluation * 0.99
                else rawMoveEvaluation

            if(board.atMove == Color.WHITE){
                bestEvaluation = max(bestEvaluation, finalMoveEvaluation)
                alpha = max(alpha, bestEvaluation)
                if(alpha >= beta){
                    return bestEvaluation
                }
            }else{
                bestEvaluation = min(bestEvaluation, finalMoveEvaluation)
                beta = min(beta, bestEvaluation)
                if(beta <= alpha){
                    return bestEvaluation
                }
            }
        }

        return bestEvaluation
    }

    class BotResult(val bestMove: MoveEvaluation, val evaluations: List<MoveEvaluation>, val count: Int){

        fun getVariance(): Double {
            val sum = evaluations.sumOf { it.evaluation }
            val avg = sum / evaluations.size
            val summedDiff = evaluations.sumOf { (it.evaluation - avg).pow(2) }
            return summedDiff / evaluations.size
        }

    }

}