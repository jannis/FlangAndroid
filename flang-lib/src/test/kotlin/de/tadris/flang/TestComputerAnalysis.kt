package de.tadris.flang

import de.tadris.flang_lib.analysis.ComputerAnalysis
import org.junit.Test

class TestComputerAnalysis {

    @Test
    fun testAnalysis(){
        val fmn = "PG2-F3 uB8-C6 HE1-D3 pE7-F6 KH1-G2 pF7-G6 HD3-F4 uC6-B5 PD2-D3 hD8-F7 KG2-H3 pG6-G5 HF4-G6 pD7-D6 PF3-F4 pG5-F4 KH3-H4 rE8-E2 FF1-E2 pF4-G3 UG1-G3 pF8-G7 PD3-D4 kA8-B8 HG6-E5 pD6-E5 UG3-G7 uB5-E2 KH4-H5 pE5-D4 KH5-G6 hF7-E5 UG7-F6 uE2-F2 KG6-G7 hE5-F7 KG7-H8"
        val analysis = ComputerAnalysis(fmn, 2) {
            println("Analysing: $it%")
        }
        val result = analysis.analyze()
        println("Analysis complete.")
        println(result.moveEvaluations.joinToString(separator = "\n"))
        println("White: ${result.white}")
        println("Black: ${result.black}")
    }

}