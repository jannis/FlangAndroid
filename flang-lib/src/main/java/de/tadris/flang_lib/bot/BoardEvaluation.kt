package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board

interface BoardEvaluation {

    fun evaluate(board: Board): Double

}