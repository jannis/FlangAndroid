package de.tadris.flang.ui.fragment

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.tadris.flang.R
import de.tadris.flang.game.AnalysisGameController
import de.tadris.flang.game.GameController
import de.tadris.flang.network.DataRepository
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.OpeningDatabaseEntry
import de.tadris.flang.ui.adapter.OpeningDatabaseEntriesAdapter
import de.tadris.flang.ui.dialog.ImportType
import de.tadris.flang.ui.dialog.openImportDialog
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class AnalysisGameFragment : AbstractAnalysisGameFragment(), OpeningDatabaseEntriesAdapter.OpeningDatabaseEntryListener {

    private var openingDatabaseVisible = false
    private var openingDatabase: RecyclerView? = null
    private val openingDatabaseAdapter = OpeningDatabaseEntriesAdapter(this, emptyList())

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = super.onCreateView(inflater, container, savedInstanceState)
        if(running){
            // Hide computer hints for running games
            binding.hintButton.visibility = View.GONE
        }else{
            binding.computerAnalysisButton.visibility = View.VISIBLE
        }
        binding.openingDatabaseToggleButton.visibility = View.VISIBLE
        binding.openingDatabaseToggleButton.setOnClickListener { toggleOpeningDatabaseView() }
        binding.analysisButton.visibility = View.GONE

        if(openingDatabaseVisible){
            addOpeningDatabaseView()
        }

        return v
    }

    override fun onDestroyView() {
        removeOpeningDatabaseView()
        openingDatabase = null
        super.onDestroyView()
    }

    private fun toggleOpeningDatabaseView(){
        if(openingDatabaseVisible){
            removeOpeningDatabaseView()
            Toast.makeText(requireContext(), R.string.openingDatabaseDisabled, Toast.LENGTH_SHORT).show()
        }else{
            addOpeningDatabaseView()
            Toast.makeText(requireContext(), R.string.openingDatabaseEnabled, Toast.LENGTH_SHORT).show()
        }
    }

    private fun addOpeningDatabaseView(){
        binding.positionName.visibility = View.GONE
        binding.gameAdditionalContentParent.visibility = View.VISIBLE

        if(openingDatabase == null){
            openingDatabase = RecyclerView(requireContext())
            openingDatabase!!.layoutManager = LinearLayoutManager(context)
            openingDatabase!!.adapter = openingDatabaseAdapter
        }
        binding.gameAdditionalContentParent.addView(openingDatabase)
        openingDatabaseVisible = true
        refreshOpeningDatabase()
    }

    private fun removeOpeningDatabaseView(){
        binding.positionName.visibility = View.VISIBLE
        binding.gameAdditionalContentParent.visibility = View.GONE

        binding.gameAdditionalContentParent.removeView(openingDatabase)
        openingDatabaseVisible = false
    }

    override fun onEntryClick(entry: OpeningDatabaseEntry) {
        val move = Move.parse(displayedBoard, entry.move)
        onMoveRequested(move)
    }

    override fun getPositionName(entry: OpeningDatabaseEntry): String? {
        val moveList = ActionList(displayedBoard.moveList.actions.toMutableList())
        moveList.addMove(entry.move)
        return positions.findCurrentPositionName(moveList.board)
    }

    override fun onDisplayedBoardChange(board: Board) {
        if(openingDatabaseVisible){
            refreshOpeningDatabase()
        }
    }

    private fun refreshOpeningDatabase(){
        lifecycleScope.launch {
            try{
                openingDatabaseAdapter.updateList(queryOpeningDatabase().result)
            }catch (e: Exception){
                // TODO show error message
                e.printStackTrace()
            }
        }
    }

    @WorkerThread
    private suspend fun queryOpeningDatabase() = withContext(Dispatchers.IO) {
        DataRepository.getInstance().accessOpenAPI().queryOpeningDatabase(displayedBoard.getFMN())
    }

    override fun isGameClickable() = true

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_analysis_to_nav_computer_analysis
    override fun getNavigationLinkToChat() = R.id.action_nav_analysis_to_nav_chat

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_analysis, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.actionImportGame){
            openImportDialog { gameString, board, type ->
                when(type){
                    ImportType.FBN2 -> {
                        baseBoard = board.clone(true)
                        firstBoard = baseBoard
                        gameBoard = baseBoard.asBaseBoardForNewBoard()
                    }
                    ImportType.FMN -> {
                        fmn = gameString
                        firstBoard = board
                        gameBoard = board.clone(true)
                    }
                }
                setDisplayedBoardToGameBoard(force = true)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}