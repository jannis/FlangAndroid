package de.tadris.flang_lib.bot

import de.tadris.flang_lib.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.pow

/**
 * For all evaluations: positive stands for white and negative for black
 *
 * This method is called "Neo method" (Matrix evaluation)
 */
open class NeoBoardEvaluation(
    protected var board: Board = Board(),
): BoardEvaluation {

    protected val evaluationMatrix = arrayOfNulls<LocationEvaluation>(Board.ARRAY_SIZE)

    init {
        for(i in evaluationMatrix.indices){
            evaluationMatrix[i] = LocationEvaluation()
        }
    }

    protected val blackStats = OverallStats()
    protected val whiteStats = OverallStats()

    protected val moveGenerator = FastMoveGenerator(board, includeOwnPieces = true, kingRange = 2)

    protected var kingsEvalNum = 0.0

    protected var pieceValueEval = 0.0
        private set

    override fun evaluate(board: Board): Double {
        this.board = board
        this.moveGenerator.board = board
        return evaluate()
    }

    open fun evaluate(): Double {
        var adder = 0.0
        var won = false
        if(board.hasWon(Color.WHITE)){
            adder = 10000.0
            won = true
        }
        if(board.hasWon(Color.BLACK)){
            adder = -10000.0
            won = true
        }
        prepare()
        board.eachXY { x, y ->
            evaluateLocation(x, y)
        }
        var evaluation = 0.0
        val movementEval = (whiteStats.movements.toDouble() / blackStats.movements) - (blackStats.movements.toDouble() / whiteStats.movements)
        pieceValueEval = (whiteStats.pieceValue.toDouble() / blackStats.pieceValue) - (blackStats.pieceValue.toDouble() / whiteStats.pieceValue)
        kingsEvalNum = if(!won) getKingsEval() else 0.0
        val matrixEval = evaluationMatrix.sumOf { it!!.evaluateField() }

        evaluation+=   5 * matrixEval
        evaluation+=  10 * movementEval
        evaluation+=  60 * pieceValueEval
        evaluation+=   1 * kingsEvalNum

        /*println("White: " + whiteStats.movements)
        println("Black: " + blackStats.movements)

        println("movment: " + movementEval)
        println("piece: " + pieceValueEval)
        println("kings: " + kingsEval)
        println("matrix: " + matrixEval)*/

        return (evaluation / 10.0) + adder
    }

    protected fun prepare(){
        evaluationMatrix.forEach {
            it?.reset()
        }
        blackStats.reset()
        whiteStats.reset()
        kingsEvalNum = 0.0
    }

    protected fun evaluateLocation(x: Int, y: Int){
        val eval = getAt(x, y)
        val boardIdx = board.getIndexOfLocation(x, y)
        val char = board.pieces[boardIdx]
        if(char == ' ') return
        val type = Type.getTypeOrNull(char)
        if(type != null){
            val color = Color.getColor(char)
            val state = PieceState.getState(board.pieces[boardIdx + 1])
            val stats = getStats(color)
            stats.pieceValue+= type.value
            eval.occupiedBy = (type.value * color.evaluationNumber).toDouble()
            moveGenerator.forEachTargetLocation(x, y, color, type, state) { targetX, targetY ->
                getAt(targetX, targetY).addThreat(color, 1.0 / type.value)
                stats.movements++
            }
        }
    }

    protected open fun getKingsEval(): Double {
        val whiteKing = board.findKingIndex(Color.WHITE)
        val blackKing = board.findKingIndex(Color.BLACK)
        if(whiteKing == -1 || blackKing == -1){
            throw IllegalStateException("Cannot find kings in board $board")
        }
        val whiteKingX = Board.getXForIndex(whiteKing)
        val whiteKingY = Board.getYForIndex(whiteKing)
        val blackKingX = Board.getXForIndex(blackKing)
        val blackKingY = Board.getYForIndex(blackKing)

        val whiteEval = 20 * 2.0.pow(whiteKingY - 6)
        val blackEval = 20 * 2.0.pow(-blackKingY + 1)

        for(y in whiteKingY until (Board.BOARD_SIZE - 1)){
            val field = getAt(whiteKingX, y)
            field.weight+= whiteEval
        }
        for(y in blackKingY downTo 1){
            val field = getAt(blackKingX, y)
            field.weight+= blackEval
        }

        return (whiteEval / blackEval) - (blackEval / whiteEval)
    }

    private fun getIndexOfLocation(x: Int, y: Int): Int {
        return (y * Board.BOARD_SIZE + x)
    }

    protected fun getAt(x: Int, y: Int): LocationEvaluation {
        return getAt(getIndexOfLocation(x, y));
    }

    private fun getAt(index: Int): LocationEvaluation {
        return evaluationMatrix[index]!!
    }

    private fun getStats(color: Color): OverallStats {
        return if(color == Color.WHITE){
            whiteStats
        }else{
            blackStats
        }
    }

    class OverallStats(var movements: Int = 1, var pieceValue: Int = 1){

        override fun toString(): String {
            return "pieces=$pieceValue, moves=$movements"
        }

        fun reset(){
            movements = 1
            pieceValue = 1
        }

    }

    class LocationEvaluation(
        var occupiedBy: Double = 0.0,
        var whiteControl: Double = 0.0,
        var blackControl: Double = 0.0,
        var weight: Double = 1.0, // multiplier
        var bonus: Double = 0.0, // bonus for the winning party
    ){

        fun addThreat(color: Color, threat: Double){
            if(color == Color.WHITE){
                whiteControl+= threat
            }else{
                blackControl+= threat
            }
        }

        fun evaluateField(): Double {
            val whiteControl = this.whiteControl + 1
            val blackControl = this.blackControl + 1
            val controlRate = (whiteControl / blackControl) - (blackControl / whiteControl)

            val result = when {
                occupiedBy > 0 -> {
                    ((1 + controlRate) * if(blackControl > whiteControl) occupiedBy else 1.0) * weight
                }
                occupiedBy < 0 -> {
                   ((-1 + controlRate) * if(whiteControl > blackControl) abs(occupiedBy) else 1.0) * weight
                }
                else -> {
                    controlRate * weight
                }
            }
            val winningColor = if(result > 0.5) 1 else if(result < -0.5) -1 else 0
            return result + winningColor * bonus
        }

        fun reset(){
            occupiedBy = 0.0
            whiteControl = 0.0
            blackControl = 0.0
            weight = 1.0
            bonus = 0.0
        }

    }

    fun printMatrix(){
        val d = evaluate()
        println("White: $whiteStats")
        println("Black: $blackStats")
        println("+-----------------+")
        for(y in 0 until Board.BOARD_SIZE){
            print("| ")
            for(x in 0 until Board.BOARD_SIZE){
                val s = (evaluationMatrix[y*8 + x]!!.evaluateField() * 2).toInt().toString()
                print(" ".repeat(max(0, 2-s.length)) + s)
            }
            println("|")
        }
        println("+-----------------+")
        println("Rating: $d")
    }

}