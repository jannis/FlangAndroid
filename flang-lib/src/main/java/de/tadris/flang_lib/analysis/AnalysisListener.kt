package de.tadris.flang_lib.analysis

fun interface AnalysisListener {

    fun onProgressChanged(progress: Int)

}