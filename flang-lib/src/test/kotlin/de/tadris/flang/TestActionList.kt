package de.tadris.flang

import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.ActionList
import org.junit.Test

import org.junit.Assert.*

class TestActionList {

    @Test
    fun addMoves(){
        val actionList = ActionList()
        actionList.addMove("PG2-G3")
        assertEquals("PG2-G3", actionList.board.getFMN())
        assertEquals(" + +P+R+H+F+U+K+ + +P+P+P+P+ +P+ + + + + + +P- + + + + + + + + + + + + + + + + + + + + + + + + +p+p+p+p+p+p+ + +k+u+f+h+r+p+ + +",
            actionList.board.getFBN())
    }

    @Test
    fun addResign(){
        val actionList = ActionList()
        actionList.addMoveList("PF2-G3 #-")
        val board = actionList.board
        assertTrue(board.gameIsComplete())
        assertTrue(board.hasWon(Color.WHITE))
        assertTrue(!board.hasWon(Color.BLACK))
    }

    @Test
    fun replayGame(){
        val actionList = ActionList()
        actionList.addMoveList("PG2-H3 pB7-B6 UG1-G5 pE7-F6 PD2-C3 kA8-B7 UG5-H5 rE8-E5 KH1-G2 kB7-A6 UH5-F4 kA6-A5 KG2-G3 pB6-C5 PH3-G4 fC8-C4 PC3-C4 kA5-A4 KG3-H4 kA4-A3 UF4-E5 kA3-A2 PE2-D3 kA2-A1")
        val board = actionList.board
        assertTrue("Game should be completed", board.gameIsComplete())
        assertEquals("k+ +P+R+H+F+ + + + +P+ + +P+ +P+ + + +P- + + + + + +P+ + + +P+K+ + +p+ +U+ + + + + + + + +p+ + +p+ +p+p+ +p+ + + +u+ +h+ +p+ + +",
            board.getFBN())
    }

}