package de.tadris.flang

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.action.ActionList
import org.junit.Test

import org.junit.Assert.*

class TestPiece {

    @Test
    fun testGetStartingPosition(){
        val board = Board.fromFMN("d2e3 d7d6 e2d3 b7a6 Ff3 kb7 d3d4 kb6 c3 d1 Hc2 ka5 d1 kb6 g1 kc6 Kf1 kb6 Ke2 kc6 Kd3 kb6 Kc4 ka5 Pb2 b2 b2 hc6 Pd5 d5 d5 e7d6 Kc4 e3 Pe3 a7b6 Pb4 hb4 Ub4 a4 Fd3 b4 Hb4")
        val piece = board.getAt(3, 2)
        assertNotNull(piece)
        assertEquals(Location(board, 5, 0), piece!!.getStartingPosition())
    }
}