package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Piece
import de.tadris.flang_lib.Vector

class Move(val piece: Piece, val target: Location) : Action {

    fun getNotation(): String {
        return piece.getChar() + piece.location.getNotation() + "-" + target.getNotation()
    }

    override fun getShortNotation(currentBoard: Board): String {
        val target = this.target.onDifferentBoard(currentBoard)
        if(target.getMovesToHere().size == 1)
            return target.getNotation().lowercase()
        return if(target.getMovesToHere().filter { it.piece.type == piece.type }.size == 1)
            piece.getChar() + target.getNotation().lowercase()
        else
            (piece.location.getNotation() + target.getNotation()).lowercase()
    }

    override fun applyToBoard(board: Board) {
        board.executeOnBoard(this)
    }

    override fun toString(): String {
        return getNotation()
    }

    override fun equals(other: Any?) = other is Move &&
            other.piece == piece &&
            other.target == target

    override fun hashCode(): Int {
        var result = piece.hashCode()
        result = 31 * result + target.hashCode()
        return result
    }

    companion object {

        fun parse(board: Board, str: String) = when(str.length){
            2 -> parseVector(str).toLocation(board).getMovesToHere().let {
                if(it.size != 1)
                    throw IllegalArgumentException("Cannot parse move '$str' -> not explicit (moves: $it)")
                return@let it.first()
            }
            3 -> parseVector(str.substring(1, 3))
                .toLocation(board)
                .getMovesToHere()
                .filter { it.piece.getChar().equals(str[0], ignoreCase = true) }
                .let {
                    if(it.size != 1)
                        throw IllegalArgumentException("Cannot parse move '$str' -> not explicit (moves: $it)")
                    return@let it.first()
                }
            4 -> parseV1(board, str)
            5 -> parseV1(board, str.substring(1, 3) + str.substring(3, 5))
            6 -> parseV1(board, str.substring(1, 3) + str.substring(4, 6))
            else -> throw IllegalArgumentException("Cannot parse move, illegal format: '$str'")
        }

        private fun parseV1(board: Board, str: String): Move {
            val from = parseVector(str.substring(0, 2)).toLocation(board)
            val to = parseVector(str.substring(2, 4)).toLocation(board)
            val piece = from.piece.value ?: throw IllegalArgumentException("Cannot move null piece at $from on board '$board'")
            return Move(piece, to)
        }

        private fun parseVector(str: String) = Vector.parse(str.uppercase()).also { it.checkValidity() }

    }
}