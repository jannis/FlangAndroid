package de.tadris.flang_lib

import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import kotlin.text.StringBuilder

class Board(val pieces: CharArray, var atMove: Color = Color.WHITE, var moveNumber: Int = 0, actionList: ActionList? = null) {

    val moveList = actionList?: ActionList(this)
    var resigned: Color? = null
    var isInfiniteGame = false

    companion object {
        const val BOARD_SIZE = 8
        const val EMPTY = ' '
        const val ARRAY_SIZE = BOARD_SIZE * BOARD_SIZE
        const val FULL_ARRAY_SIZE = ARRAY_SIZE * 2

        const val DEFAULT_BOARD = "  PRHFUK" +
                "  PPPPPP" +
                "        " +
                "        " +
                "        " +
                "        " +
                "pppppp  " +
                "kufhrp  "

        private fun parse(str: String): CharArray{
            return when (str.length) {
                FULL_ARRAY_SIZE -> {
                    str.toCharArray()
                }
                ARRAY_SIZE -> {
                    val sb = StringBuilder()
                    str.forEach {
                        sb.append(it)
                        sb.append(PieceState.NORMAL.char)
                    }
                    sb.toString().toCharArray()
                }
                else -> {
                    throw IllegalArgumentException("Size of a board must be $FULL_ARRAY_SIZE (size was ${str.length})")
                }
            }
        }

        fun fromFMN(fmn: String, base: Board? = null) = ActionList(null, base).addMoveList(fmn).board

        fun fromFBN2(fbn2: String): Board {
            val builder = StringBuilder()
            var atMove = Color.WHITE
            var digitBuilder = ""
            "$fbn2?".forEachIndexed { index, c ->
                if (index == 0) {
                    atMove = when(c){
                        '+' -> Color.WHITE
                        '-' -> Color.BLACK
                        else -> throw IllegalArgumentException("Cannot parse FBNv2 '$fbn2' -> must start with + or -")
                    }
                    return@forEachIndexed
                }
                if(c.isDigit()){
                    digitBuilder+= c
                    if(digitBuilder.length >= 3)
                        throw IllegalArgumentException("Cannot parse FBNv2 '$fbn2' -> invalid number $digitBuilder")
                }else if(digitBuilder.isNotEmpty()){
                    for(x in 0 until digitBuilder.toInt()){
                        builder.append(" +")
                    }
                    digitBuilder = ""
                }
                if (c.isLetter()) {
                    builder.append(c)
                    builder.append('+')
                }
                if(c == '-'){
                    builder.deleteAt(builder.length - 1)
                    builder.append('-')
                }
            }
            return Board(builder.toString(), atMove)
        }

        fun getYForIndex(index: Int) = (index / 2) / BOARD_SIZE

        fun getXForIndex(index: Int) = (index / 2) % BOARD_SIZE

    }

    constructor(str: String, atMove: Color = Color.WHITE) : this(parse(str), atMove)

    constructor() : this(DEFAULT_BOARD)

    fun getAt(index: Int): Piece? {
        if(index < 0){ return null }
        val char = pieces[index]
        return if(char == EMPTY){
            null
        }else{
            Piece(getLocationForIndex(index), char, pieces[index + 1])
        }
    }

    fun getAt(loc: Vector): Piece? {
        return getAt(getIndexOfLocation(loc))
    }

    fun getAt(x: Int, y: Int) = getAt(getIndexOfLocation(x, y))

    fun getIndexOfLocation(x: Int, y: Int): Int {
        return (y * BOARD_SIZE + x) * 2
    }

    fun getIndexOfLocation(loc: Vector): Int {
        return getIndexOfLocation(loc.x, loc.y)
    }

    fun getLocationForIndex(index: Int): Location {
        val halfIndex = index / 2
        return Location(this, halfIndex % BOARD_SIZE, halfIndex / BOARD_SIZE)
    }

    fun gameIsComplete(): Boolean{
        return !isInfiniteGame && (hasWon(Color.WHITE) || hasWon(Color.BLACK) || resigned != null)
    }

    fun getWinningColor() = when {
        hasWon(Color.WHITE) -> Color.WHITE
        hasWon(Color.BLACK) -> Color.BLACK
        else -> null
    }

    fun hasWon(color: Color): Boolean {
        if(color.getOpponent() == resigned) return true // If opponent has resigned, it's a win
        val index = findKingIndex(color)
        if(index == -1) return false // has no king, so hasn't won
        return if(findKingIndex(color.getOpponent()) == -1){
            true // opponent has no king, so won
        }else Companion.getYForIndex(index) == color.winningY
    }

    fun findKing(color: Color): Piece? {
        return findFigure(Type.KING, color)
    }

    fun findKingIndex(color: Color) = findIndex(Type.KING, color)

    fun findFigure(type: Type, color: Color): Piece? {
        return getAt(findIndex(type, color))
    }

    fun findIndex(type: Type, color: Color): Int {
        return pieces.indexOf(type.getChar(color))
    }

    inline fun eachLocation(action: (Location) -> Unit){
        for(y in 0 until BOARD_SIZE){
            for(x in 0 until BOARD_SIZE){
                action(Location(this, x, y))
            }
        }
    }

    inline fun eachXY(action: (x: Int, y: Int) -> Unit){
        for(y in 0 until BOARD_SIZE){
            for(x in 0 until BOARD_SIZE){
                action(x, y)
            }
        }
    }

    inline fun eachPiece(color: Color?, action: (Piece) -> Unit){
        eachLocation {
            val piece = it.piece.value
            if(piece != null){
                if(color == null || piece.color == color){
                    action(piece)
                }
            }
        }
    }

    fun revertableExecuteOnBoard(move: Move): Pair<Piece?, Int> {
        val previousPiece = getAt(move.target)
        val unfrozenPiece = findUnfrozenFigure(move.piece.color)
        executeOnBoard(move)
        return previousPiece to unfrozenPiece
    }

    fun executeOnBoard(move: Move): Board {
        val piece = move.piece
        unfreezeAllOnBoard(piece.color)
        clearOnBoard(piece.location)
        setOnBoard(move.target, piece)
        atMove = atMove.getOpponent()
        moveNumber++
        moveList.addMoveWithoutApply(move)
        return this
    }

    fun revertMove(move: Move, revertInfo: Pair<Piece?, Int>){
        setOnBoard(move.piece.location, move.piece, freezeMethod = FreezeMethod.UNFREEZE)

        val previousPiece = revertInfo.first
        val freezeMethod = if(previousPiece?.state == PieceState.FROZEN) FreezeMethod.FORCE else FreezeMethod.UNFREEZE
        setOnBoard(move.target, previousPiece, freezeMethod)

        val frozenPiece = revertInfo.second
        if(frozenPiece != -1) pieces[frozenPiece * 2 + 1] = PieceState.FROZEN.char

        atMove = atMove.getOpponent()
        moveNumber--
        moveList.removeLastMoveWithoutApply()
    }

    fun unfreezeAllOnBoard(color: Color){
        val pos = findUnfrozenFigure(color)
        if(pos != -1) pieces[pos * 2 + 1] = PieceState.NORMAL.char
    }

    fun findUnfrozenFigure(color: Color): Int {
        val upperCase = color == Color.WHITE
        for(index in 0 until FULL_ARRAY_SIZE step 2){
            if(pieces[index + 1] == PieceState.FROZEN.char){
                if(pieces[index].isUpperCase() == upperCase){
                    return index / 2
                }
            }
        }
        return -1
    }

    fun executeOnNewBoard(move: Move): Board {
        return clone(false).executeOnBoard(move)
    }

    fun asBaseBoardForNewBoard() = clone(true).also { it.moveList.base = this }

    fun clone(slowButComplete: Boolean): Board {
        return if(slowButComplete){
            val board = Board(pieces.clone(), atMove, moveNumber, moveList.clone())
            board.isInfiniteGame = isInfiniteGame
            board
        }else{
            Board(pieces.clone(), atMove, moveNumber)
        }
    }

    fun clearOnBoard(location: Location){
        setOnBoard(location, null)
    }

    fun setOnBoard(location: Location, piece: Piece?, freezeMethod: FreezeMethod = FreezeMethod.AUTO){
        val index = getIndexOfLocation(location)
        if(piece != null){
            pieces[index] = piece.getChar()
            pieces[index+1] = (
                    if(freezeMethod != FreezeMethod.UNFREEZE){
                        if(freezeMethod == FreezeMethod.FORCE || piece.type.hasFreeze){
                            PieceState.FROZEN
                        }else{
                            PieceState.NORMAL
                        }
                    }else {
                        PieceState.NORMAL
                    }).char
            if(piece.type == Type.PAWN &&
                    (if(piece.color == Color.WHITE) BOARD_SIZE-1-location.y else location.y) == 0){
                setOnBoard(location, Piece(location, Type.UNI, piece.color, piece.state), FreezeMethod.AUTO)
            }
        }else{
            pieces[index] = EMPTY
            pieces[index+1] = PieceState.NORMAL.char
        }
    }

    fun findAllMoves(color: Color): List<Move>{
        if(gameIsComplete()){
            return emptyList()
        }
        val list = mutableListOf<Move>()

        eachPiece(color) {
            list.addAll(it.getPossibleMoves())
        }

        return list
    }

    fun getFBN(): String { // Flang board notation - contains current state
        return pieces.concatToString()
    }

    fun getFBN2(): String {
        val builder = StringBuilder()
        builder.append(if(atMove == Color.WHITE) '+' else '-')
        var spaceCount = 0
        (getFBN() + "?").forEach {
            if(it == ' '){
                spaceCount++
            }else if(spaceCount > 0 && it != '+'){
                builder.append(spaceCount.toString())
                spaceCount = 0
            }
            if(it.isLetter() || it == '-'){
                builder.append(it)
            }
        }
        return builder.toString()
    }

    fun getFMN(): String { // Flang move notation - contains all data
        return moveList.toString()
    }

    fun getFMN2(): String { // FMNv2 - shorter than the normal FMN
        return moveList.toShortString()
    }

    val hasCustomBase get() = moveList.base != null

    override fun toString(): String {
        return getFBN()
    }

    fun printBoard(){
        println("+-----------------+")
        for(y in BOARD_SIZE -1 downTo 0){
            print("| ")
            for(x in 0 until BOARD_SIZE){
                print(pieces[(y*8 + x)*2] + " ")
            }
            println("|")
        }
        println("+-----------------+")
    }

    enum class FreezeMethod {
        AUTO, // Depending on piece type
        FORCE, // Force freeze piece
        UNFREEZE // Dont freeze
    }
}