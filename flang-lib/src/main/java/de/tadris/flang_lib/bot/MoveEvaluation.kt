package de.tadris.flang_lib.bot

import de.tadris.flang_lib.action.Move

class MoveEvaluation(val move: Move, var evaluation: Double, val depth: Int){

    override fun toString(): String {
        return "[$move -> $evaluation#$depth]"
    }

    fun getNotation(): String {
        return "$move->${evaluation.toFloat()}"
    }

}