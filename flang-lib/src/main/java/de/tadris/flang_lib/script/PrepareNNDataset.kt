package de.tadris.flang_lib.script

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.action.Resign
import de.tadris.flang_lib.bot.FlangBot
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import java.io.File
import kotlin.math.absoluteValue

/**
 * This generates a dataset that can be used to train ML models. The output CSV contains:
 * - FBN, AtMove (1: White, 0: Black), MoveCount, Win Prob (1: White, -1: Black), List of action probs (64x64)
 *
 */
fun main(){
    val bot = FlangBot(3, 3, evaluation = NeoBoardEvaluation())

    val games = loadAllGames()
        .also { println("Loaded ${it.size} games") }
        .filter { it.gameIsComplete() && it.moveList.actions.last() !is Resign }
        .also { println("Filtered ${it.size} games") }

    val output = File("doc/training_data.csv")
    output.createNewFile()
    val writer = output.bufferedWriter()

    games.forEachIndexed { index, finalBoard ->
        println("Game $index/${games.size}")
        val winningColor = finalBoard.getWinningColor()!!
        finalBoard.moveList.forEachGameState { board, nextAction ->
            if(!board.gameIsComplete()){
                writer.appendLine(generateLineFromBoard(board, bot, winningColor))
            }
        }
    }

    writer.close()
}

private fun loadAllGames(): List<Board> {
    val games = mutableListOf<Board>()
    File("doc/accuracy_ezragt.txt").forEachLine { line ->
        if(!line.startsWith("|1") && !line.startsWith("|0")) return@forEachLine
        val fmn = line.replace("|1", "").replace("|0", "").replace("|", "").trim()
        games += Board.fromFMN(fmn)
    }
    return games
}

private fun generateLineFromBoard(board: Board, bot: FlangBot, winColor: Color): String {
    val result = bot.findBestMove(board, printTime = false)
    val evals = result.evaluations
    val evalProbs = evals.map {
        encodeAction(it.move) to 1 - ((result.bestMove.evaluation - it.evaluation).absoluteValue / 3).coerceIn(0.0, 0.99) // 5 is the maximum difference
    }
    val moveProbs = FloatArray(64*64)
    evalProbs.forEach { (move, prob) ->
        moveProbs[move] = prob.toFloat()
    }

    val line = StringBuilder()
    line.append(board.getFBN())
    line.append(",")
    line.append(if(board.atMove == Color.WHITE) 1 else 0)
    line.append(",")
    line.append(board.moveNumber)
    line.append(",")
    line.append(if(winColor == Color.WHITE) 1 else -1)
    moveProbs.forEach {
        line.append(",")
        line.append(it)
    }

    return line.toString()
}

private fun encodeAction(move: Move): Int {
    val isFlipped = move.piece.color == Color.BLACK
    val start = if(isFlipped) move.piece.location.flip() else move.piece.location
    val end = if(isFlipped) move.target.flip() else move.target

    val startEnc = start.y * Board.BOARD_SIZE + start.x
    val endEnc = end.y * Board.BOARD_SIZE + end.x

    return endEnc * Board.ARRAY_SIZE + startEnc
}

private fun Location.flip() = Location(
    board,
    Board.BOARD_SIZE - 1 - x,
    Board.BOARD_SIZE - 1 - y,
)