package de.tadris.flang_lib

import de.tadris.flang_lib.bot.BlankBoardEvaluation
import de.tadris.flang_lib.bot.FlangBot

fun main(){

    val maxDepth = 4
    val board = Board.fromFBN2("-2PRHF4PPPP1P5P9K4r1U-3u1h-p2pppp1p2k1f2p2")
    val eval = BlankBoardEvaluation()
    fun Board.findBestMove(depth: Int) =
        FlangBot(depth, depth, evaluation = eval).findBestMove(this, printTime = false)

    val result = board.findBestMove(4)
    result.evaluations.forEachIndexed { index, moveEvaluation ->
        print("${index + 1}. ${moveEvaluation.move}")
        val boardCopy = board.clone(true)
        boardCopy.executeOnBoard(moveEvaluation.move)
        for(depth in (maxDepth - 1) downTo 1){
            val nextMove = boardCopy.findBestMove(depth).bestMove.move
            print(" -> $nextMove")
            boardCopy.executeOnBoard(nextMove)
        }
        println()
    }

}