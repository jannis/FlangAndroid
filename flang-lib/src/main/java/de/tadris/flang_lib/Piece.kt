package de.tadris.flang_lib

import de.tadris.flang_lib.action.Move

class Piece(val location: Location, val type: Type, val color: Color, val state: PieceState) {

    constructor(location: Location, pieceChar: Char, stateChar: Char)
            : this(location, Type.getType(pieceChar), Color.getColor(pieceChar), PieceState.getState(stateChar))

    fun getPossibleMoves(): List<Move> {
        val moves = mutableListOf<Move>()

        getPossibleTargetLocations().forEach {
            val piece = it.piece.value
            if(piece == null || piece.color != color){
                // Cannot move onto own piece
                moves.add(Move(this, it))
            }
        }
        return moves
    }

    fun getPossibleTargetLocations(): List<Location> {
        val targets = mutableListOf<Location>()

        getPossibleDirections().forEach { batch ->
            getPossibleTargetLocationForBatch(batch).forEach {
                if(!type.hasDoubleMoves || !targets.contains(it)){
                    targets.add(it)
                }
            }
        }
        return targets
    }

    private fun getPossibleTargetLocationForBatch(batch: List<Vector>): List<Location> {
        val targets = mutableListOf<Location>()

        batch.forEach {
            val target = location.add(it)
            if(target.isValid()){
                targets.add(target)
                if(target.piece.value != null){
                    return targets
                }
            }else{
                return targets
            }
        }
        return targets
    }

    fun getPossibleDirections(): List<List<Vector>> {
        if(state == PieceState.FROZEN){
            // No moves possible if frozen
            return emptyList()
        }
        val moveBatches = mutableListOf<List<Vector>>()

        type.moves.forEach { typeMoves ->
            val moves = mutableListOf<Vector>()
            typeMoves.forEach {
                moves.add(
                    if(color == Color.WHITE) it else it.reverseY()
                )
            }
            moveBatches.add(moves)
        }

        return moveBatches
    }

    fun getStartingPosition(): Location {
        var currentLocation = location
        location.board.moveList.actions.asReversed().forEach { action ->
            if(action is Move){
                if(action.target == currentLocation){
                    currentLocation = action.piece.location
                }
            }
        }
        return currentLocation
    }

    fun getChar(): Char {
        return type.getChar(color)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Piece

        if (type != other.type) return false
        if (color != other.color) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + color.hashCode()
        return result
    }

    override fun toString(): String {
        return "$color $type at $location"
    }
}