package de.tadris.flang.util

import android.content.Context
import de.tadris.flang.R
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.ActionList
import kotlin.concurrent.thread

class Positions(val context: Context) {

    private var isLoaded = false
    private val positions = mutableMapOf<String, String>()

    init {
        thread {
            loadFile()
            isLoaded = true
        }
    }

    private fun loadFile(){
        context.resources.openRawResource(R.raw.positions)
            .bufferedReader()
            .readLines()
            .forEach { readLine(it) }
    }

    private fun readLine(line: String){
        if(line.isEmpty()) return
        val data = line.split(":")
        positions[data[1]] = data[0].trim()
    }

    fun findCurrentPositionName(board: Board) = findPositionName(board.getFBN())

    fun findPositionName(board: Board): String? {
        val actions = ActionList(board.moveList.actions.toMutableList())
        var first = true
        while (actions.actions.isNotEmpty()){
            if(first){
                first = false
            }else{
                actions.rewind()
            }
            val name = findPositionName(actions.board.getFBN())
            if(name != null){
                return name
            }
        }
        return null
    }

    fun findPositionName(fbn: String) = if(isLoaded) positions[fbn] else null

}