package de.tadris.flang.network_api.model

import de.tadris.flang_lib.Board

data class GameAttachment(val id: String, val fmn: String, val fbn: String){

    val isOnlineGame get() = id != "-1" && id.isNotEmpty()

    val board get() = if(fmn.isNotEmpty()){
        Board.fromFMN(fmn)
    } else {
        Board.fromFBN2(fbn)
    }

}