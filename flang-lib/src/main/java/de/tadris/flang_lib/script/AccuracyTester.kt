package de.tadris.flang_lib

import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.FlangBot
import java.io.File
import java.util.concurrent.Executors
import kotlin.math.roundToInt

fun main(){
    println("Loading file")
    val file = File("doc/accuracy_ezragt.txt")
    val tests = mutableListOf<AccuracyTest>()
    file.readText().lines().forEach { line ->
        if(line.startsWith("|")){
            tests.addAll(AccuracyTestBatch(line).getTests())
        }
    }
    println("Loaded ${tests.size} tests")
    val reducedTests = tests.filterIndexed { index, _ -> index % 50 == 0 }
    println("Picked ${reducedTests.size} tests")

    val lock = Object()
    var testCount = 0
    var successCount = 0
    var errorCount = 0.0

    val executor = Executors.newFixedThreadPool(24)

    for(test in reducedTests){
        executor.submit {
            val bot = FlangBot(4, 4)
            val action = bot.findBestMove(test.board.clone(true), printTime = false)
            synchronized(lock){
                if(action.bestMove.move.getNotation() == test.expectedAction.getNotation()){
                    successCount++
                }else{
                    errorCount+= action.evaluations.indexOfFirst {
                        it.move.getNotation() == test.expectedAction.getNotation()
                    }
                }
                testCount++
            }
        }
    }
    while (testCount < reducedTests.size){
        print("\rTested $testCount of ${reducedTests.size}")
        Thread.sleep(200)
    }
    executor.shutdown()
    println("\rTested $testCount of ${reducedTests.size}")
    val accuracy = (successCount.toDouble() * 100 / testCount).roundToInt()
    val avgError = errorCount.toFloat() / testCount
    val score = (successCount.toDouble() / testCount / avgError * 1000).roundToInt()
    println("===============================")
    println("Results:")
    println("- Tests: $successCount/$testCount")
    println("- Accuracy: $accuracy%")
    println("- Average Error: $avgError")
    println("- Score: $score")
    println("===============================")
}

class AccuracyTestBatch(val isWhite: Boolean, val fmn: String){

    constructor(line: String) : this(
        line.split("|")[1].trim().toInt() == 1,
        line.split("|")[2].trim()
    )

    fun getTests(): List<AccuracyTest> {
        val actionList = ActionList().addMoveList(fmn)
        val tests = mutableListOf<AccuracyTest>()
        val offset = if(isWhite) 0 else 1
        for(i in 0..(actionList.size - 2)){
            if((i + offset) % 2 == 0){
                val action = actionList[i]
                if(action is Move){
                    val tmpActionList = ActionList()
                    tmpActionList.addMoves(actionList.actions.subList(0, i))
                    tests.add(AccuracyTest(tmpActionList.regenerateBoard(), action))
                }
            }
        }
        return tests
    }

}

class AccuracyTest(val board: Board, val expectedAction: Move){

    override fun toString(): String {
        return board.getFMN() + " -> " + expectedAction.toString()
    }
}