package de.tadris.flang_lib.bot

import de.tadris.flang_lib.*
import kotlin.math.*

/**
 * For all evaluations: positive stands for white and negative for black
 *
 * This method is called "Neo method" (Matrix evaluation)
 */
open class StageEvaluation2(
    protected var board: Board = Board(),
): BoardEvaluation {

    companion object {
        const val DEFENDING_POINTS_THRESHOLD = 2
    }

    private val evaluationMatrix = arrayOfNulls<LocationEvaluation>(Board.ARRAY_SIZE)

    init {
        for(i in evaluationMatrix.indices){
            evaluationMatrix[i] = LocationEvaluation()
        }
    }

    private val blackStats = OverallStats()
    private val whiteStats = OverallStats()

    private val moveGenerator = FastMoveGenerator(board, includeOwnPieces = true, kingRange = 2)

    private var blackKingX = 0
    private var blackKingY = 0
    private var whiteKingX = 0
    private var whiteKingY = 0

    private var won = false
    private var kingsEvalNum = 0.0
    private var pieceValueEval = 0.0

    override fun evaluate(board: Board): Double {
        this.board = board
        this.moveGenerator.board = board
        return evaluate()
    }

    private fun evaluate(): Double {
        prepare()
        evaluateMatrix()
        return when(getGameStage()){
            GameStage.EARLY -> evaluateEarlyGame()
            GameStage.MID -> defaultEvaluate()
            GameStage.END -> evaluateEndgame()
        }
    }

    private fun evaluateMatrix(){
        board.eachXY { x, y ->
            evaluateLocation(x, y)
        }
    }

    private fun evaluateEarlyGame(): Double {
        var pawnsBonus = 0.0

        board.pieces.forEachIndexed { index, char ->
            if(char.lowercaseChar() != Type.PAWN.c) return@forEachIndexed

            val isWhite = char.isUpperCase()
            val colorFactor = if(isWhite) 1 else -1
            val x = Board.getXForIndex(index)
            val y = Board.getYForIndex(index)

            val borderX = if(isWhite) 7 else 0
            val borderY = if(isWhite) 0 else 7

            val xDistance = (x - borderX).absoluteValue.toDouble()
            val yDistance = (y - borderY).absoluteValue.toDouble()

            pawnsBonus += colorFactor * exp(-xDistance) * (sqrt(yDistance) - 1) // It's good to push pawns on the king's side
        }

        return defaultEvaluate() + pawnsBonus + 4 * pieceValueEval
    }

    private fun evaluateEndgame(): Double {
        val defenderDiff = whiteStats.defenderCount - blackStats.defenderCount

        return defaultEvaluate() + 2.5 * kingsEvalNum + 5 * defenderDiff
    }

    private fun defaultEvaluate(): Double {
        val adder = (board.getWinningColor()?.evaluationNumber ?: 0) * 10000.0

        var evaluation = 0.0
        pieceValueEval = (whiteStats.pieceValue.toDouble() / blackStats.pieceValue) - (blackStats.pieceValue.toDouble() / whiteStats.pieceValue)
        kingsEvalNum = if(!won) getKingsEval() else 0.0
        val matrixEval = evaluationMatrix.sumOf { it!!.evaluateField() }

        evaluation +=   5 * matrixEval
        evaluation +=  60 * pieceValueEval
        evaluation +=   1 * kingsEvalNum

        return (evaluation / 10.0) + adder
    }

    private fun prepare(){
        evaluationMatrix.forEach {
            it?.reset()
        }
        blackStats.reset()
        whiteStats.reset()
        kingsEvalNum = 0.0

        won = if(board.hasWon(Color.WHITE)){
            true
        }else board.hasWon(Color.BLACK)

        prepareKingsPos()
    }

    private fun prepareKingsPos(){
        if(won) return
        val whiteKing = board.findKingIndex(Color.WHITE)
        val blackKing = board.findKingIndex(Color.BLACK)
        if(whiteKing == -1 || blackKing == -1){
            throw IllegalStateException("Cannot find kings in board $board")
        }
        whiteKingX = Board.getXForIndex(whiteKing)
        whiteKingY = Board.getYForIndex(whiteKing)
        blackKingX = Board.getXForIndex(blackKing)
        blackKingY = Board.getYForIndex(blackKing)
    }

    private fun evaluateLocation(x: Int, y: Int){
        val eval = getAt(x, y)
        val boardIdx = board.getIndexOfLocation(x, y)
        val char = board.pieces[boardIdx]
        if(char == ' ') return
        val type = Type.getTypeOrNull(char)
        if(type != null){
            val color = Color.getColor(char)
            val state = PieceState.getState(board.pieces[boardIdx + 1])
            val stats = getStats(color)
            var defendingPoints = 0
            stats.pieceValue+= type.value
            eval.occupiedBy = (type.value * color.evaluationNumber).toDouble()
            moveGenerator.forEachTargetLocation(x, y, color, type, state) { targetX, targetY ->
                getAt(targetX, targetY).addThreat(color, 1.0 / type.value)
                if(color == Color.WHITE && isInBlackKingsSight(x, y)) defendingPoints++
                if(color == Color.BLACK && isInWhiteKingsSight(x, y)) defendingPoints++
            }

            if(defendingPoints > DEFENDING_POINTS_THRESHOLD){
                // this piece is a defender
                getStats(color).defenderCount++
            }
        }
    }

    protected open fun getKingsEval(): Double {
        val whiteEval = 20 * 2.0.pow(whiteKingY - 6)
        val blackEval = 20 * 2.0.pow(-blackKingY + 1)

        for(y in whiteKingY until (Board.BOARD_SIZE - 1)){
            val field = getAt(whiteKingX, y)
            field.weight+= whiteEval
        }
        for(y in blackKingY downTo 1){
            val field = getAt(blackKingX, y)
            field.weight+= blackEval
        }

        return (whiteEval / blackEval) - (blackEval / whiteEval)
    }

    private fun getIndexOfLocation(x: Int, y: Int): Int {
        return (y * Board.BOARD_SIZE + x)
    }

    private fun getAt(x: Int, y: Int): LocationEvaluation {
        return getAt(getIndexOfLocation(x, y));
    }

    private fun getAt(index: Int): LocationEvaluation {
        return evaluationMatrix[index]!!
    }

    private fun getStats(color: Color): OverallStats {
        return if(color == Color.WHITE){
            whiteStats
        }else{
            blackStats
        }
    }

    private fun isInBlackKingsSight(x: Int, y: Int): Boolean {
        return y != 0 && (x - blackKingX).absoluteValue <= blackKingY - y && (blackKingY - y).absoluteValue <= 4
    }

    private fun isInWhiteKingsSight(x: Int, y: Int): Boolean {
        return y != 7 && (x - whiteKingX).absoluteValue <= y - whiteKingY && (whiteKingY - y).absoluteValue <= 4
    }

    fun getGameStage(): GameStage {
        var pawns = 0
        var unis = 0
        var total = 0

        board.pieces.forEach {
            when(it.lowercaseChar()) {
                CHAR_PAWN -> pawns++
                CHAR_UNI -> unis++
            }
            if(it != CHAR_NONE){
                total++
            }
        }

        return when {
            pawns <= 9 || total + unis - pawns <= 7 -> GameStage.END
            pawns <= 12 || total + unis - pawns <= 9 -> GameStage.MID
            else -> GameStage.EARLY
        }
    }

    fun evaluateBreakdown() = BoardEvaluationBreakdown(evaluationMatrix)

    enum class GameStage {
        EARLY,
        MID,
        END
    }

    class OverallStats(var pieceValue: Int = 1, var defenderCount: Int = 0){

        override fun toString(): String {
            return "pieces=$pieceValue defenderCount=$defenderCount"
        }

        fun reset(){
            pieceValue = 1
            defenderCount = 0
        }

    }

    class LocationEvaluation(
        var occupiedBy: Double = 0.0,
        var whiteControl: Double = 0.0,
        var blackControl: Double = 0.0,
        var weight: Double = 1.0, // multiplier
        var bonus: Double = 0.0, // bonus for the winning party
    ){

        fun addThreat(color: Color, threat: Double){
            if(color == Color.WHITE){
                whiteControl+= threat
            }else{
                blackControl+= threat
            }
        }

        fun evaluateField(): Double {
            val whiteControl = this.whiteControl + 1
            val blackControl = this.blackControl + 1
            val controlRate = (whiteControl / blackControl) - (blackControl / whiteControl)

            val result = when {
                occupiedBy > 0 -> {
                    ((1 + controlRate) * if(blackControl > whiteControl) occupiedBy else 1.0) * weight
                }
                occupiedBy < 0 -> {
                   ((-1 + controlRate) * if(whiteControl > blackControl) abs(occupiedBy) else 1.0) * weight
                }
                else -> {
                    controlRate * weight
                }
            }
            val winningColor = if(result > 0.5) 1 else if(result < -0.5) -1 else 0
            return result + winningColor * bonus
        }

        fun reset(){
            occupiedBy = 0.0
            whiteControl = 0.0
            blackControl = 0.0
            weight = 1.0
            bonus = 0.0
        }

    }

    class BoardEvaluationBreakdown(val evaluationMatrix: Array<LocationEvaluation?>)

}