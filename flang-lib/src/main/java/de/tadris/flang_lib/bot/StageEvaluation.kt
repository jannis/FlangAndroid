package de.tadris.flang_lib.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.CHAR_NONE
import de.tadris.flang_lib.Type
import kotlin.math.absoluteValue
import kotlin.math.exp
import kotlin.math.sqrt

class StageEvaluation(board: Board = Board()) : NeoBoardEvaluation(board) {

    override fun evaluate(): Double {
        return when(getGameStage()){
            GameStage.EARLY -> evaluateEarlyGame()
            GameStage.MID -> super.evaluate()
            GameStage.END -> evaluateEndgame()
        }
    }

    private fun evaluateEarlyGame(): Double {
        var pawnsBonus = 0.0

        board.pieces.forEachIndexed { index, char ->
            if(char.lowercaseChar() != Type.PAWN.c) return@forEachIndexed

            val isWhite = char.isUpperCase()
            val colorFactor = if(isWhite) 1 else -1
            val x = Board.getXForIndex(index)
            val y = Board.getYForIndex(index)

            val borderX = if(isWhite) 7 else 0
            val borderY = if(isWhite) 0 else 7

            val xDistance = (x - borderX).absoluteValue.toDouble()
            val yDistance = (y - borderY).absoluteValue.toDouble()

            pawnsBonus += colorFactor * exp(-xDistance) * (sqrt(yDistance) - 1) // It's good to push pawns on the king's side
        }

        return super.evaluate() + pawnsBonus + 4 * pieceValueEval
    }

    private fun evaluateEndgame(): Double {
        return super.evaluate() + 2.5 * kingsEvalNum
    }

    fun getGameStage(): GameStage {
        var pawns = 0
        var unis = 0
        var total = 0

        board.pieces.forEach {
            when(it.lowercaseChar()) {
                Type.PAWN.c -> pawns++ // TODO make type characters const values
                Type.UNI.c -> unis++
            }
            if(it != CHAR_NONE){
                total++
            }
        }

        return when {
            pawns <= 9 || total + unis - pawns <= 7 -> GameStage.END
            pawns <= 12 || total + unis - pawns <= 9 -> GameStage.MID
            else -> GameStage.EARLY
        }
    }

    enum class GameStage {
        EARLY,
        MID,
        END
    }

}