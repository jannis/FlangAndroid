package de.tadris.flang_lib.action

import de.tadris.flang_lib.Board

sealed interface Action {

    fun applyToBoard(board: Board)

    override fun toString(): String

    fun getShortNotation(currentBoard: Board): String

}