package de.tadris.flang.network_api.model

data class ChatMessages(val messages: List<Message>)
