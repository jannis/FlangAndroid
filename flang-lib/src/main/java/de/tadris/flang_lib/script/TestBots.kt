package de.tadris.flang_lib.script
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.bot.*
import kotlin.system.exitProcess

fun main() {

    val board = Board(Board.DEFAULT_BOARD)

    //val board = Board.fromFMN("b2 g7 b3 g6 g2g3 uc6 Uf3 d7d6 e2d3 f3 Ff3 fd5 Kg2 f8 Fe4 e4 Kf3 hc6 Ke4 b7b6 Kf3 b7 Kg2 ka6 Ph3 pa5 g4 c7b6 Pf3 he5 g3 pb5 a1 pb6 Pf4 c6")

    //val board = Board(" + + + + + + + + + + + + +P+P+ + + + +P+ + + + + + + +P+F+ + +K+ +p+ + + + +u- + +p+ +p+ + +p+ + +k+ + +p+ +p+ + + + + + + + + +")

        /*de.tadris.flang_lib.Board("K       " +
            "PPPPPPPP" +
            "        " +
            "        " +
            "        " +
            "      p " +
            "pppppp p" +
            "       k")*/

    // Bot 1 is white, bot2 is black

    val bot1 = FlangBot(5, 5, evaluation = NeoBoardEvaluation())
    val bot2 = FlangBot(5, 5, evaluation = StageEvaluation())

    var sum1 = 0
    var sum2 = 0

    board.printBoard()

    fun printAndExit(){
        val winningColor = when {
            board.hasWon(Color.WHITE) -> "White"
            board.hasWon(Color.BLACK) -> "Black"
            else -> "None"
        }
        println("$winningColor has won")
        println("Eval 1: $sum1 / Eval 2: $sum2")
        println(board.getFMN2())
        exitProcess(0)
    }

    NeoBoardEvaluation(board).printMatrix()

    while (true){
        board.printBoard()
        println(board.getFBN())
        NeoBoardEvaluation(board).printMatrix()
        println(StageEvaluation(board).getGameStage())

        println("BOT 1")
        val result = bot1.findBestMove(board)
        println(result.evaluations)
        sum1+= bot1.evaluations
        val move1 = result.bestMove.move
        println("Evaluated moves: ${bot1.evaluations}")
        println("Move: $move1")
        board.executeOnBoard(move1)

        if(board.gameIsComplete()) printAndExit()

        board.printBoard()
        println(board.getFBN())
        NeoBoardEvaluation(board).printMatrix()
        println(StageEvaluation(board).getGameStage())

        println("BOT 2")
        val result2 = bot2.findBestMove(board)
        println(result.evaluations)
        sum2+= bot2.evaluations
        val move2 = result2.bestMove.move
        println("Evaluated moves: ${bot2.evaluations}")
        println("Move: $move2")
        board.executeOnBoard(move2)

        if(board.gameIsComplete()) printAndExit()

        board.printBoard()
        println(board.getFBN())
        NeoBoardEvaluation(board).printMatrix()
    }

}