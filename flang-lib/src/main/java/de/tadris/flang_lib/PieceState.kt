package de.tadris.flang_lib

enum class PieceState(val char: Char){

    NORMAL('+'),
    FROZEN('-');


    companion object {

        fun getState(char: Char) =
            if(char == '-') FROZEN else NORMAL

    }

}