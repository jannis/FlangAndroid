package de.tadris.flang

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.bot.HeatmapEvaluation
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import org.junit.Assert.assertEquals
import org.junit.Test

class TestHeatMap {

    @Test
    fun testHeatmapAvg(){
        HeatmapEvaluation.heatmaps.forEach {
            println("${it.key}: ${it.value.sumOf { it.sum() } / Board.ARRAY_SIZE.toDouble()}")
        }
    }

    @Test
    fun testEvaluationWorks(){
        val board = Board.fromFMN("PG2-H3 pB7-B6 UG1-G5 pE7-F6 PD2-C3 kA8-B7 UG5-H5 rE8-E5 KH1-G2 kB7-A6 UH5-F4 kA6-A5 KG2-G3 pB6-C5 PH3-G4 fC8-C4 PC3-C4")
        val evaluation = HeatmapEvaluation()
        println(evaluation.evaluate(board))
    }

    @Test
    fun testEvaluationZero(){
        val board = Board()
        val evaluation = HeatmapEvaluation()
        assertEquals(0.0, evaluation.evaluate(board), 0.2)
    }

}