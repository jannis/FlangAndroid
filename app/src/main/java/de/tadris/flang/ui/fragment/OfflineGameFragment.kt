package de.tadris.flang.ui.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import de.tadris.flang.R
import de.tadris.flang.game.GameController
import de.tadris.flang.game.OfflineBotGameController
import de.tadris.flang_lib.bot.*

class OfflineGameFragment : GameFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        root.findViewById<View>(R.id.player1InfoParent).setOnClickListener {
            openStrengthChooseDialog()
        }
        root.findViewById<View>(R.id.player2InfoParent).setOnClickListener {
            openStrengthChooseDialog()
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openStrengthChooseDialog()
    }

    private fun openStrengthChooseDialog(){
        val arrayAdapter = ArrayAdapter<String>(requireActivity(), android.R.layout.select_dialog_item)
        val options = mutableMapOf(
            OfflineBotGameController.NAME + "#1" to Pair({ NeoBoardEvaluation() }, 1),
            OfflineBotGameController.NAME + "#3" to Pair({ NeoBoardEvaluation() }, 3),
            OfflineBotGameController.NAME + "#4" to Pair({ NeoBoardEvaluation() }, 4),
            OfflineBotGameController.NAME + "#5" to Pair({ NeoBoardEvaluation() }, 5),
            "BlankBot#3" to Pair({ BlankBoardEvaluation() }, 3),
            "StageBot#4" to Pair({ StageEvaluation() }, 4),
            "StageBot#5" to Pair({ StageEvaluation() }, 5),
        )
        arrayAdapter.addAll(options.keys)
        AlertDialog.Builder(activity)
            .setTitle(R.string.offlineChooseStrength)
            .setAdapter(arrayAdapter) { _, which ->
                val option = options.entries.toList()[which]
                val selectedConfiguration = option.value
                (gameController as? OfflineBotGameController)
                    ?.updateConfiguration(option.key, selectedConfiguration.first, selectedConfiguration.second)
            }
            .show()
    }

    override fun createGameController(): GameController {
        return OfflineBotGameController(requireActivity())
    }

    override fun getNavigationLinkToAnalysis() = R.id.action_nav_offline_game_to_nav_analysis
    override fun getNavigationLinkToChat() = R.id.action_nav_offline_game_to_nav_chat
}