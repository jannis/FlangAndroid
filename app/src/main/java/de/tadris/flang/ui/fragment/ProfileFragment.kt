package de.tadris.flang.ui.fragment

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import de.tadris.flang.R
import de.tadris.flang.databinding.FragmentProfileBinding
import de.tadris.flang.network.CredentialsStorage
import de.tadris.flang.network.DataRepository
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.User
import de.tadris.flang.ui.adapter.GameAdapter
import de.tadris.flang.ui.view.ChartFormatter
import de.tadris.flang.ui.view.ChartFormatter.initChart
import de.tadris.flang.util.applyTo
import de.tadris.flang.util.getThemePrimaryColor
import de.tadris.flang.util.getThemeTextColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment : Fragment(R.layout.fragment_profile), GameAdapter.GameAdapterListener {

    companion object {
        const val ARGUMENT_USERNAME = "username"
        private const val PAGE_SIZE = 10
    }

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!


    private lateinit var gameAdapter: GameAdapter
    private lateinit var username: String

    private var user: User? = null

    private var loading = false
    private var reachedTheEnd = false
    private var currentOffset = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        username = arguments?.getString(ARGUMENT_USERNAME) ?: CredentialsStorage(requireContext()).getUsername()
        loadNewAdapter()
    }

    private fun loadNewAdapter(){
        currentOffset = 0
        reachedTheEnd = false
        gameAdapter = GameAdapter(mutableListOf(), this)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)!!
        _binding = FragmentProfileBinding.bind(root)

        val layoutManager = if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            GridLayoutManager(requireContext(), 2)
        }else{
            LinearLayoutManager(requireContext())
        }
        binding.profileGamesRecyclerView.layoutManager = layoutManager
        binding.profileGamesRecyclerView.adapter = gameAdapter

        root.findViewById<NestedScrollView>(R.id.nestedScrollView).setOnScrollChangeListener { v: ViewGroup, _, scrollY, _, _ ->
            if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight)){
                loadMore()
            }
        }

        binding.profileSwipeToRefresh.setOnRefreshListener {
            loadNewAdapter()
            binding.profileGamesRecyclerView.adapter = gameAdapter
            loadMore()
        }

        (activity as AppCompatActivity).supportActionBar?.title = username

        if(gameAdapter.itemCount < 5){
            loadMore()
        }

        with(binding.profileChart) {
            with(ChartFormatter){
                initChart(requireActivity())
                applyDateFormatter()
            }
            description.text = getString(R.string.ratingHistory)
        }

        loadUserInfo()

        return root
    }

    private fun loadMore(){
        if(loading){ return }
        if(reachedTheEnd){ return }
        loading = true
        lifecycleScope.launch {
            try{
                binding.profileSwipeToRefresh.isRefreshing = true
                val newGames = findGames()
                currentOffset+= PAGE_SIZE
                if(newGames.games.isEmpty()){
                    reachedTheEnd = true
                }
                gameAdapter.appendGames(newGames.games)
            }catch (e: Exception){
                e.printStackTrace()
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
            }finally {
                binding.profileSwipeToRefresh.isRefreshing = false
                loading = false
            }
        }
    }

    private fun loadUserInfo(){
        lifecycleScope.launch {
            try{
                user = getUserInfo()
                updateInfoAndChart()
            }catch (e: Exception){
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun updateInfoAndChart(){
        updateInfo()
        updateChart()
    }

    private fun updateInfo(){
        val user: User = this.user!!
        user.applyTo(binding.userTitle, binding.userUsername, binding.userRating)
        binding.userSummary.text = getString(R.string.userSummary,
            SimpleDateFormat.getDateInstance().format(Date(user.registration)),
            user.completedGames)
    }

    private fun updateChart(){
        val chart = binding.profileChart
        val user: User = this.user!!
        if(user.history.size < 3){
            chart.visibility = View.GONE
            return
        }else{
            chart.visibility = View.VISIBLE
        }
        val ratingDataSet = LineDataSet(
            user.history.map { Entry(it.date.toFloat(), it.rating) },
            getString(R.string.rating))

        ChartFormatter.fillChart(requireActivity(), chart, ratingDataSet)
    }

    @WorkerThread
    private suspend fun getUserInfo() = withContext(Dispatchers.IO) {
        DataRepository.getInstance().accessOpenAPI().findUser(username)
    }

    @WorkerThread
    private suspend fun findGames() = withContext(Dispatchers.IO) {
        DataRepository.getInstance().accessOpenAPI().findGames(username, pageSize = PAGE_SIZE, offset = currentOffset)
    }

    override fun onClick(gameInfo: GameInfo) {
        val bundle = Bundle()
        bundle.putLong(OnlineGameFragment.EXTRA_GAME_ID, gameInfo.gameId)
        findNavController().navigate(R.id.action_nav_profile_to_nav_game, bundle)
    }

}