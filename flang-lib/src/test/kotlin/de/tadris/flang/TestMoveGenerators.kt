package de.tadris.flang

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Type
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.FastMoveGenerator
import org.junit.Assert.*
import org.junit.Test

class TestMoveGenerators {

    @Test
    fun testMoveGenerator(){
        val board = Board()
        assertEquals(22, board.findAllMoves(Color.WHITE).size)
    }

    @Test
    fun testFastMoveGenerator(){
        val finalBoard = Board.fromFMN("Uf3 uc6 Hd3 a7b6 Kg1 ka7 Pb2 c2 Rc1 pa6 e2e3 a4 g2g3 d7e6 Kg2 d3 d3 b2 Rc2 g7 e3d4 ub5 Pc4 pc6 Kh3 uf5 a3 f7g6 f5 f5 b4 he6 e5 c6c5 a4 hg5 c5 c8 Kh4 c5 d7 h8 f4 a8 Kg3 he4 Kf3 rb8 Pf5 f2 Rf2 f5 e4 rb7 Kf5 a7 d8 kb6 b2 kb5 d5 a4 b7 a3 g3 c4 Re7 a2 Ke6 a1")
        var count = 0
        finalBoard.moveList.forEachGameState { board, _ ->
            val fastMoveGenerator = FastMoveGenerator(board, false)
            board.eachPiece(null) { piece ->
                val realMoves = piece.getPossibleMoves().sortedBy { it.toString() }
                val fastMoves = mutableListOf<Move>()
                fastMoveGenerator.forEachTargetLocation(piece.location.x, piece.location.y, piece.color, piece.type, piece.state){ x, y ->
                    fastMoves.add(Move(piece, Location(board, x, y)))
                }
                fastMoves.sortBy { it.toString() }
                assertArrayEquals("Piece $piece needs the same moves", realMoves.toTypedArray(), fastMoves.toTypedArray())
                count++
            }
        }
        println("Checked $count moves")
    }

}