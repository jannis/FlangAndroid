package de.tadris.flang.ui.board

import android.view.View
import de.tadris.flang_lib.Location

interface FieldView {

    fun isFullOverlay(): Boolean = false

    fun getLocation(): Location

    fun getView(): View

}