package de.tadris.flang_lib.analysis

enum class AnalysisMoveType {
    GOOD,
    MISTAKE,
    BLUNDER,
    RESIGN
}