package de.tadris.flang.game

import android.app.Activity
import android.widget.Toast
import de.tadris.flang.network.CredentialsStorage
import de.tadris.flang.network.DataRepository
import de.tadris.flang.ui.dialog.LoadingDialogViewController
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import java.lang.Exception
import kotlin.concurrent.thread

open class OnlineGameController(activity: Activity, gameId: Long) : AbstractGameController(activity) {

    var gameId = gameId
        protected set

    private var moves = 0
    private var board = Board(Board.DEFAULT_BOARD)
    private var gameFinished = false
    protected var fragmentPaused = false

    override fun requestGame() {
        val dialog = LoadingDialogViewController(activity)
        thread {
            try {
                initGame()
                startGameRefreshThread()
            } catch (e: Exception){
                e.printStackTrace()
                showError(e.message)
                Thread.sleep(3000)
                activity.runOnUiThread {
                    requestGame()
                }
            } finally {
                dialog.hide()
            }
        }
    }

    private fun initGame(){
        val info = DataRepository.getInstance().accessOpenAPI().getGameInfo(gameId)
        val username = CredentialsStorage(activity).getUsername()
        val isWhite = username == info.white.username
        val isBlack = username == info.black.username
        val color: Color? = if(isWhite && isBlack){ null } else if(isWhite){ Color.WHITE } else{ Color.BLACK }
        board = info.toBoard()
        moves = board.moveList.size
        activity.runOnUiThread {
            callback.onGameRequestSuccess(info, isWhite || isBlack, color)
        }
    }

    private fun startGameRefreshThread(){
        fragmentPaused = false
        gameFinished = false
        thread {
            while (!board.gameIsComplete() && !fragmentPaused && !gameFinished){
                try{
                    val info = DataRepository.getInstance().accessOpenAPI().getGameInfo(gameId, moves)
                    if(!info.running){
                        gameFinished = true
                    }
                    if(fragmentPaused){
                        continue
                    }
                    activity.runOnUiThread {
                        callback.onUpdate(info)
                    }
                    board = info.toBoard()
                    if(board.moveList.size >= moves){
                        board.moveList.actions.subList(moves, board.moveList.size).forEach { action ->
                            activity.runOnUiThread {
                                callback.onUpdate(action)
                            }
                        }
                        moves = board.moveList.size
                    }else{
                        // we know more moves than the server, so we give him a bit time to keep up
                        Thread.sleep(200)
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                    showError(e.message)
                    Thread.sleep(5000)
                }
            }
        }
    }

    override fun stop() {
        fragmentPaused = true
    }

    override fun resume() {
        fragmentPaused = false
        startGameRefreshThread()
    }

    override fun resignGame() {
        asyncAction { DataRepository.getInstance().accessRestrictedAPI(activity).resign(gameId) }
    }

    override fun onMoveRequested(move: Move, newBoardRequest: Board?) {
        asyncAction { DataRepository.getInstance().accessRestrictedAPI(activity).executeMove(gameId, move) }
    }

    private fun asyncAction(action: () -> Unit){
        thread {
            try{
                action()
            }catch(e: Exception){
                e.printStackTrace()
                showError(e.message)
            }
        }
    }

    protected fun showError(message: String?){
        activity.runOnUiThread {
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun isCreativeGame() = false

}