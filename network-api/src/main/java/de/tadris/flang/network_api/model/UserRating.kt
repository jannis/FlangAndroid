package de.tadris.flang.network_api.model

data class UserRating(val date: Long, val rating: Float)
