package de.tadris.flang.network_api.model

data class GameRequestResult(val gameId: Long)