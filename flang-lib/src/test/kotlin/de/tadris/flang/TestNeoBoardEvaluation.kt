package de.tadris.flang

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.bot.NeoBoardEvaluation
import org.junit.Test

class TestNeoBoardEvaluation {

    @Test
    fun testEvaluationWorks(){
        val board = Board.fromFMN("PG2-H3 pB7-B6 UG1-G5 pE7-F6 PD2-C3 kA8-B7 UG5-H5 rE8-E5 KH1-G2 kB7-A6 UH5-F4 kA6-A5 KG2-G3 pB6-C5 PH3-G4 fC8-C4 PC3-C4")
        val evaluation = NeoBoardEvaluation(board)
        println(evaluation.evaluate())
    }

}